Welcome to VarClass
===================== 

ReadTheDocs : https://varclass.readthedocs.io/en/latest/index.html#

Installing VC
------------

`git clone https://weber8thomas@bitbucket.org/weber8thomas/variantsclassification.git`

Python 3.7 Requirements : 
* cloudpickle==0.5.5
* matplotlib==2.2.3
* numpy==1.15.1
* pandas==0.23.4
* pathlib==1.0.1
* plotly==3.4.2
* pprint==0.1
* pyfiglet
* scikit-learn==0.20.2
* termcolor
* tqdm==4.26

Use VarClass
-------------

There are four modes in VarClass
- Training and Testing mode
- Training mode
- Testing mode
- Prediction mode


Training and Testing mode
--------------------------


The aim of this mode is to TRAIN and TEST the different selected algorithms on selected data



    python VarClass.py --train_and_test \
                       -i Data/varibench_preprocess.csv.gz \



    python VarClass.py --train_and_test \
                        --input Data/varibench_preprocess.csv.gz \
                        --output_dir Output_Test \
                        --full_clf \
                        --cross_validation 5 \
                        --threads -1



    python VarClass.py --train_and_test \
                       --input Data/varibench_preprocess.csv.gz \
                       --output_dir Output_Test \
                       --ratio 0.2 \
                       --proportion 0.9 \
                       --full_clf \
                       --grid \
                       --cross_validation_grid_search 5 \
                       --n_iteration 20 \
                       --threshold 0.15 \
                       --threads -1


Testing mode
--------------------------

The aim of this mode is to TEST the different selected algorithms on selected file by using models generated on similar data.

