from itertools import cycle
from pprint import pprint
from sklearn import metrics
from VarClass_utils import utils, ML
import collections
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure
import numpy as np
import operator
import os
import pandas as pd
import plotly
import plotly.graph_objs as go
import plotly.plotly as py
import seaborn as sns

plt.ioff()
plt.style.use('ggplot')


def plot_accuracy(log, output_dir):
    """
    Method for plot accuracy comparison between algorithms

    Args:
        log: Pandas Dataframe
            Dataframe with metrics
        output_dir: str
            Directory where will be save the plots
    Returns:
        None
    """
    output_dir = output_dir + "/Plots/Comparative_plots"
    utils.mkdir(output_dir)
    fig_acc = plt.figure()
    sns.set_color_codes("muted")
    sns.barplot(x='Accuracy', y='Classifier', data=log)
    plt.xlabel('Accuracy')
    plt.title('Classifier Accuracy')
    # plt.show()
    plt.savefig(output_dir + "/Accuracy.png", bbox_inches='tight', dpi=100)
    plt.close(fig_acc)


def plot_log_loss(log, output_dir):
    """
    Method for plot log loss comparison between algorithms

    Args:
        log: Pandas Dataframe
            Dataframe with metrics
        output_dir: str
            Directory where will be save the plots
    Returns:
        None
    """
    output_dir = output_dir + "/Plots/Comparative_plots"
    utils.mkdir(output_dir)
    fig_ll = plt.figure()
    sns.set_color_codes("muted")
    sns.barplot(x='Log Loss', y='Classifier', data=log)
    plt.xlabel('Log Loss')
    plt.title('Classifier Log Loss')
    # plt.show()
    plt.savefig(output_dir + "/Log_Loss.png", bbox_inches='tight', dpi=100)
    plt.close(fig_ll)


def plot_auc_score(log, output_dir):
    """
    Method for plot AUC score comparison between algorithms

    Args:
        log: Pandas Dataframe
            Dataframe with metrics
        output_dir: str
            Directory where will be save the plots
    Returns:
        None
    """
    output_dir = output_dir + "/Plots/Comparative_plots"
    utils.mkdir(output_dir)
    fig_auc = plt.figure()
    sns.set_color_codes("muted")
    sns.barplot(x='AUC Score', y='Classifier', data=log)
    plt.xlabel('AUC Score')
    plt.title('Classifier AUC Score')
    # plt.show()
    plt.savefig(output_dir + "/AUC_Score.png", bbox_inches='tight', dpi=100)
    plt.close(fig_auc)




def plot_roc_curve_training(dict_y_test, results_proba, output_dir):
    """
    Method for plot ROC comparison between algorithms

    Args:
        dict_y_test: dict
            Store the y_test used for each iteration of CV
        results_proba : dict
            Store the proba obtained for each iteration of CV for every algorithm used
        output_dir: str
            Directory where will be save the plots
    Returns:
        None
    """
    # output_dir = output_dir + '/Plots/ROC_plots'
    output_dir = output_dir + '/Plots'
    utils.mkdir(output_dir)
    dict_auc = dict()
    ordered_dict = collections.defaultdict(dict)
    for algo, results in results_proba.items():
        fig_algo = figure(num=algo, figsize=(12, 9), dpi=80, facecolor='w', edgecolor='k')
        plt.figure(algo)
        tprs = list()
        aucs = list()
        mean_fpr = np.linspace(0, 1, 100)
        for index, arrays in results.items():
            fpr, tpr, thresholds = metrics.roc_curve(dict_y_test[index], arrays)
            tprs.append(np.interp(mean_fpr, fpr, tpr))
            tprs[-1][0] = 0.0
            roc_auc = metrics.auc(fpr, tpr)
            aucs.append(roc_auc)

            plt.plot(fpr, tpr, lw=1, alpha=0.3, )
            # label='ROC fold %d (AUC = %0.2f)' % (index, roc_auc))

        mean_tpr = np.mean(tprs, axis=0)
        mean_tpr[-1] = 1.0
        mean_auc = metrics.auc(mean_fpr, mean_tpr)

        std_auc = np.std(aucs)
        plt.plot(mean_fpr, mean_tpr, color='red',
                 label=r'Mean ROC (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
                 lw=2, alpha=.8)
        std_tpr = np.std(tprs, axis=0)
        tprs_upper = np.minimum(mean_tpr + std_tpr, 1)
        tprs_lower = np.maximum(mean_tpr - std_tpr, 0)

        dict_auc[algo] = mean_auc

        ordered_dict[algo]['mean_tpr'] = mean_tpr
        ordered_dict[algo]['mean_fpr'] = mean_fpr
        ordered_dict[algo]['std_auc'] = std_auc
        ordered_dict[algo]['std_tpr'] = std_tpr
        ordered_dict[algo]['tprs_upper'] = tprs_upper
        ordered_dict[algo]['tprs_lower'] = tprs_lower

        plt.fill_between(mean_fpr, tprs_lower, tprs_upper, color='grey', alpha=.2,
                         label=r'$\pm$ 1 std. dev.')
        plt.xlim([-0.05, 1.05])
        plt.ylim([-0.05, 1.05])
        plt.xlabel('False Positive Rate')
        plt.ylabel('True Positive Rate')
        plt.title('Receiver operating curve : ' + str(algo) + ' | ' + str(index) + ' fold cross-validation')
        plt.legend(loc="lower right")

        plt.savefig(output_dir + "/ROC_" + str(algo) + ".png", bbox_inches='tight', dpi=100)

        plt.close(fig_algo)


    fig_glob = figure(num='Global', figsize=(12, 9), dpi=80, facecolor='w', edgecolor='k')
    plt.figure('Global')

    # for prediction in predicted_labels.columns:
    #     fpr, tpr, thresholds = metrics.roc_curve(labels, predicted_labels[prediction])
    #     roc_auc = metrics.auc(fpr, tpr)
    #     predictor = prediction.split('_')
    #     predictor = [x for x in predictor if x][2]
    #     dict_auc[predictor] = roc_auc
    #     ordered_dict[predictor]['mean_tpr'] = tpr
    #     ordered_dict[predictor]['mean_fpr'] = fpr

        # plt.plot(fpr, tpr, label=r'ROC : ' + str(predictor) + ' - AUC = %0.2f' % (roc_auc), lw=2, alpha=.8)

    ordered_dict_auc = sorted(dict_auc.items(), key=operator.itemgetter(1), reverse=True)


    for elem in ordered_dict_auc:
        algo = elem[0]
        mean_auc = elem[1]

        try:

            plt.plot(ordered_dict[algo]['mean_fpr'], ordered_dict[algo]['mean_tpr'],
                     label=r'Mean ROC : ' + str(algo) + ' - AUC = %0.2f $\pm$ %0.2f)' % (
                         mean_auc, ordered_dict[algo]['std_auc']),
                     lw=2, alpha=.8)
        except:
            plt.plot(ordered_dict[algo]['mean_fpr'], ordered_dict[algo]['mean_tpr'],
                     label=r'Mean ROC : ' + str(algo) + ' - AUC = %0.2f' % (
                         mean_auc),
                     lw=2, alpha=.8)
        # plt.fill_between(ordered_dict[algo]['mean_fpr'], ordered_dict[algo]['tprs_lower'],
        #                  ordered_dict[algo]['tprs_upper'], alpha=.2,
                         # label=r'$\pm$ ' + str(algo) + '1 std. dev.'
                         # )
        plt.xlim([-0.05, 1.05])
        plt.ylim([-0.05, 1.05])
        plt.xlabel('False Positive Rate')
        plt.ylabel('True Positive Rate')
        plt.title('Receiver operating curve : Global results | ' + str(index) + ' fold cross-validation')
        plt.legend(loc="lower right")
    plt.savefig(output_dir + "/ROC_Summary.png", bbox_inches='tight', dpi=100)
    plt.close(fig_glob)


def plot_precision_recall_curve_testing(y_test, results_proba, output_dir):
    """
    Method for plot PR comparison between algorithms

    Args:
        y_test:
        dict_y_test: dict
            Store the y_test used for each iteration of CV
        results_proba : dict
            Store the proba obtained for each iteration of CV for every algorithm used
        output_dir: str
            Directory where will be save the plots
    Returns:
        None
    """
    """
    Method for plot ROC comparison between algorithms

    Args:
        dict_y_test: dict
            Store the y_test used for each iteration of CV
        results_proba : dict
            Store the proba obtained for each iteration of CV for every algorithm used
        output_dir: str
            Directory where will be save the plots
    Returns:
        None
    """
    # output_dir = output_dir + '/Plots/PR_plots'
    output_dir = output_dir + '/Plots'
    utils.mkdir(output_dir)
    dict_auc = dict()
    ordered_dict = collections.defaultdict(dict)
    ordered_dict_auc = collections.defaultdict()

    for algo, results in results_proba.items():
        precision, recall, thresholds = metrics.precision_recall_curve(y_test, results)
        pr_auc = metrics.average_precision_score(y_test, results)

        ordered_dict_auc[algo] = pr_auc
        ordered_dict[algo]['precision'] = precision
        ordered_dict[algo]['recall'] = recall

    ordered_dict_auc = sorted(ordered_dict_auc.items(), key=operator.itemgetter(1), reverse=True)

    fig_glob = figure(num='Global_PR', figsize=(12, 9), dpi=80, facecolor='w', edgecolor='k')

    for elem in ordered_dict_auc:
        algo = elem[0]
        auc = elem[1]

        plt.figure('Global_PR')
        plt.plot(ordered_dict[algo]['precision'], ordered_dict[algo]['recall'],
                 label= str(algo) + ' (AUC = %0.2f )'
                       % (auc), lw=2, alpha=.8)
        # plt.fill_between(ordered_dict[algo]['mean_fpr'], ordered_dict[algo]['tprs_lower'],
        #                  ordered_dict[algo]['tprs_upper'], alpha=.2,
                         # label=r'$\pm$ ' + str(algo) + '1 std. dev.'
                         # )
        plt.xlim([-0.05, 1.05])
        plt.ylim([-0.05, 1.05])
        plt.xlabel('Precision')
        plt.ylabel('Recall')
        plt.title('Precision Recall curve : Evaluation results')
        plt.legend(loc="lower left", title='Method (AUC)')
    plt.savefig(output_dir + "/PR_Evaluation.png", bbox_inches='tight', dpi=100)

    plt.close(fig_glob)

def plot_roc_curve_testing(y_test, results_proba, output_dir, predicted_labels, labels):
    """
    Method for plot ROC comparison between algorithms

    Args:
        y_test:
        results_proba : dict
            Store the proba obtained for each iteration of CV for every algorithm used
        output_dir: str
            Directory where will be save the plots
    Returns:
        None
    """
    # output_dir = output_dir + '/Plots/ROC_plots'
    output_dir = output_dir + '/Plots'

    utils.mkdir(output_dir)
    dict_auc = dict()
    ordered_dict = collections.defaultdict(dict)

    for algo, results in results_proba.items():
        fpr, tpr, thresholds = metrics.roc_curve(y_test, results)
        roc_auc = metrics.auc(fpr, tpr)

        dict_auc[algo] = roc_auc
        ordered_dict[algo]['tpr'] = tpr
        ordered_dict[algo]['fpr'] = fpr


    fig_glob = figure(num='Global', figsize=(18, 12), dpi=120, facecolor='w', edgecolor='k')
    plt.figure('Global')

    for prediction in predicted_labels.columns:
        fpr, tpr, thresholds = metrics.roc_curve(labels, predicted_labels[prediction])
        roc_auc = metrics.auc(fpr, tpr)
        predictor = prediction
        # predictor = prediction.split('_')
        # predictor = [x for x in predictor if x][2]
        dict_auc[predictor] = roc_auc
        ordered_dict[predictor]['mean_tpr'] = tpr
        ordered_dict[predictor]['mean_fpr'] = fpr

        # plt.plot(fpr, tpr, label=r'ROC : ' + str(predictor) + ' - AUC = %0.2f' % (roc_auc), lw=2, alpha=.8)

    ordered_dict_auc = sorted(dict_auc.items(), key=operator.itemgetter(1), reverse=True)
    pprint(ordered_dict_auc)
    exit()
    for elem in ordered_dict_auc:
        algo = elem[0]
        mean_auc = elem[1]


        plt.plot(ordered_dict[algo]['fpr'], ordered_dict[algo]['tpr'],
                 label= str(algo) + ' (%0.2f)' % (mean_auc), lw=2, alpha=.8)
        plt.xlim([-0.05, 1.05])
        plt.ylim([-0.05, 1.05])
        plt.xlabel('False Positive Rate')
        plt.ylabel('True Positive Rate')
        plt.title('Receiver operating curve : Evaluation results')
        plt.legend(loc="lower right", title='Method (AUC)')
    plt.savefig(output_dir + "/ROC_Evaluation.png", bbox_inches='tight', dpi=100)


def print_stdout(log, input, output_dir, predicted_labels=None, labels=None, data=None):
    """
    Print to standard output and save results table to csv and html table

    Args:
        predicted_labels:
        labels:
        data:
        input:
        log: Pandas Dataframe
            Dataframe with the metrics (ROC-AUC, PR-AUC, Accuracy ...)
            linked to each scenario
        output_dir: str
            Name of the output directory

    Returns:
        None

    """

    # print('=' * 100)
    # print('=' * 46, 'RESULTS', '=' * 45)
    # print('=' * 100)
    if predicted_labels is not None and labels is not None and data is not None:
        tmp_list = list()
        log_list = list()
        for prediction in predicted_labels.keys():
            l = ML.compute_metrics(prediction, predicted_labels[prediction], data[prediction].values, labels)
            l['Classifier'] = prediction.split('_')[0]
            log_list.append(l)
        log_mean = pd.concat(log_list)
        log_mean = log_mean.sort_values(by=['Accuracy'], ascending=False).reset_index(drop=True)
        final_df = pd.concat([log, log_mean], axis=0)
    # print(log)
    # print(log_mean)
        print(final_df.reset_index(drop=True))
        # output_dir = output_dir + "/Reports"
        output_dir = output_dir


        name_html = "/Classification_summary.html"
        name_csv = "/Classification_summary.csv"

        pd.set_option('display.max_colwidth', -1)

        final_df.to_csv(str(output_dir) + str(name_csv), sep='\t', index=False)
        utils.write_to_html_file(final_df, title="Benchmark - " + os.path.basename(input), filename=str(output_dir) + str(name_html))
        print('=' * 100)
        print('=' * 100)
        print('\n' * 3)
