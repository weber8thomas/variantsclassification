import pandas as pd
import numpy as np
from sklearn import preprocessing
from sklearn.model_selection import KFold, GridSearchCV



def plotly_roc_curve(dict_y_test, results_proba, output_dir):
    """
    Method for plot ROC comparison between algorithms

    Args:
        dict_y_test: dict
            Store the y_test used for each iteration of CV
        results_proba : dict
            Store the proba obtained for each iteration of CV for every algorithm used
        output_dir: str
            Directory where will be save the plots
    Returns:
        None
    """
    output_dir = output_dir + '/Plots/ROC_plots'
    utils.mkdir(output_dir)
    dict_auc = dict()
    ordered_dict = collections.defaultdict(dict)
    # colors = cycle(['cyan', 'indigo', 'seagreen', 'yellow', 'blue', 'darkorange'])
    lw = 2
    data_algo = list()
    for algo, results in results_proba.items():
        i = 0
        plt.figure(algo)
        tprs = list()
        aucs = list()
        data = list()
        mean_fpr = np.linspace(0, 1, 100)
        for index, arrays in results.items():
            fpr, tpr, thresholds = metrics.roc_curve(dict_y_test[index], arrays)
            tprs.append(np.interp(mean_fpr, fpr, tpr))
            tprs[-1][0] = 0.0
            roc_auc = metrics.auc(fpr, tpr)
            aucs.append(roc_auc)
            trace = go.Scatter(x=fpr, y=tpr,
                               mode='lines',
                               line=dict(width=lw),
                               name='ROC fold %d (area = %0.2f)' % (i, roc_auc))            # label='ROC fold %d (AUC = %0.2f)' % (index, roc_auc))
            data.append(trace)
            i += 1

        trace = go.Scatter(x=[0, 1], y=[0, 1],
                           mode='lines',
                           line=dict(width=lw, color='black', dash='dash'),
                           name='Luck')
        data.append(trace)

        mean_tpr = np.mean(tprs, axis=0)
        mean_tpr[-1] = 1.0
        mean_auc = metrics.auc(mean_fpr, mean_tpr)
        dict_auc[algo] = mean_auc
        std_auc = np.std(aucs)
        std_tpr = np.std(tprs, axis=0)
        tprs_upper = np.minimum(mean_tpr + std_tpr, 1)
        tprs_lower = np.maximum(mean_tpr - std_tpr, 0)


        ordered_dict[algo]['mean_tpr'] = mean_tpr
        ordered_dict[algo]['mean_fpr'] = mean_fpr
        ordered_dict[algo]['std_auc'] = std_auc
        ordered_dict[algo]['std_tpr'] = std_tpr
        ordered_dict[algo]['tprs_upper'] = tprs_upper
        ordered_dict[algo]['tprs_lower'] = tprs_lower

        trace = go.Scatter(x=mean_fpr, y=mean_tpr,
                           mode='lines',
                           line=dict(width=lw, color='green', dash='dash'),
                           name='Mean ROC (area = %0.2f)' % mean_auc)
        data.append(trace)

        layout = go.Layout(title='Receiver operating characteristic for algo : '+str(algo),
                           xaxis=dict(title='False Positive Rate', showgrid=False,
                                      range=[-0.05, 1.05]),
                           yaxis=dict(title='True Positive Rate', showgrid=False,
                                      range=[-0.05, 1.05]))
        fig = go.Figure(data=data, layout=layout)
        plotly.offline.plot(fig, filename=output_dir + "/ROC_" + str(algo) + ".html")

    ordered_dict_auc = sorted(dict_auc.items(), key=operator.itemgetter(1), reverse=True)

    plotly_roc_mean(ordered_dict, ordered_dict_auc,data_algo, output_dir)


def plotly_roc_mean(ordered_dict, ordered_dict_auc,data_algo, output_dir, lw=2):
    for elem in ordered_dict_auc:
        algo = elem[0]
        mean_auc = elem[1]

        trace = go.Scatter(x=ordered_dict[algo]['mean_fpr'], y=ordered_dict[algo]['mean_tpr'],
                           mode='lines',
                           line=dict(width=lw, dash='dash'),
                           name='Mean ROC ' + str(algo) + '(area = %0.2f)' % mean_auc)

        data_algo.append(trace)

    layout = go.Layout(title='Receiver operating characteristic example',
                       xaxis=dict(title='False Positive Rate', showgrid=False,
                                  range=[-0.05, 1.05]),
                       yaxis=dict(title='True Positive Rate', showgrid=False,
                                  range=[-0.05, 1.05]))
    fig_mean = go.Figure(data=data_algo, layout=layout)

    plotly.offline.plot(fig_mean, filename=output_dir + "/ROC_summary.html")



def plot_precision_recall_curve(dict_y_test, results_proba, output_dir):
    """
    Method for plot PR comparison between algorithms

    Args:
        dict_y_test: dict
            Store the y_test used for each iteration of CV
        results_proba : dict
            Store the proba obtained for each iteration of CV for every algorithm used
        output_dir: str
            Directory where will be save the plots
    Returns:
        None
    """
    output_dir = output_dir + '/Plots/PR_plots'
    utils.mkdir(output_dir)

    for algo, data in results_proba.items():
        fig_algo = figure(num=algo, figsize=(12, 9), dpi=80, facecolor='w', edgecolor='k')
        plt.figure(algo)

        tprs = list()
        aucs = list()
        mean_fpr = np.linspace(0, 1, 100)

        for index, arrays in data.items():
            precision, recall, thresholds = metrics.precision_recall_curve(dict_y_test[index], arrays)
            tprs.append(np.interp(mean_fpr, precision, recall))
            # tprs[-1][0] = 0.0
            pr_auc = metrics.average_precision_score(dict_y_test[index], arrays)
            aucs.append(pr_auc)
            plt.plot(precision, recall, lw=1, alpha=0.3, )
            # label='PR fold %d (AUC = %0.2f)' % (index, pr_auc))

        mean_tpr = np.mean(tprs, axis=0)
        # mean_tpr[-1] = 0
        mean_auc = metrics.auc(mean_fpr, mean_tpr)
        std_auc = np.std(aucs)
        plt.plot(mean_fpr, mean_tpr, color='b',
                 label=r'Mean PR (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
                 lw=2, alpha=.8)
        std_tpr = np.std(tprs, axis=0)
        tprs_upper = np.minimum(mean_tpr + std_tpr, 1)
        tprs_lower = np.maximum(mean_tpr - std_tpr, 0)
        plt.fill_between(mean_fpr, tprs_lower, tprs_upper, color='grey', alpha=.2,
                         label=r'$\pm$ 1 std. dev.')

        plt.xlim([-0.05, 1.05])
        plt.ylim([-0.05, 1.05])
        plt.xlabel('Precision')
        plt.ylabel('Recall')
        plt.title('Precision Recall curve : ' + str(algo) + ' | ' + str(index) + ' fold cross-validation')
        plt.legend(loc="lower left")
        plt.savefig(output_dir + "/PR_" + str(algo) + ".png", bbox_inches='tight', dpi=100)
        plt.close(fig_algo)

        fig_glob = figure(num='Global', figsize=(12, 9), dpi=80, facecolor='w', edgecolor='k')
        plt.figure('Global')
        plt.plot(mean_fpr, mean_tpr,
                 label=r'Mean PR : + ' + str(algo) + '(AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
                 lw=2, alpha=.8)
        std_tpr = np.std(tprs, axis=0)
        tprs_upper = np.minimum(mean_tpr + std_tpr, 1)
        tprs_lower = np.maximum(mean_tpr - std_tpr, 0)
        # plt.fill_between(mean_fpr, tprs_lower, tprs_upper, color='grey', alpha=.2,
        #                  label=r'$\pm$ 1 std. dev.')

        plt.xlim([-0.05, 1.05])
        plt.ylim([-0.05, 1.05])
        plt.xlabel('Precision')
        plt.ylabel('Recall')
        plt.title('Precision Recall curve : Global results | ' + str(index) + ' fold cross-validation')
        plt.legend(loc="lower left")
    plt.savefig(output_dir + "/PR_Summary.png", bbox_inches='tight', dpi=100)
    plt.close(fig_glob)

    # plt.show()

def remove_label_test(y):
    """

    :type y: Pandas Dataframe
    :param y: Testing dataset
    :return: returns a dataframe without label
    """
    return y.drop('True_Label', axis=1)


def splitting_dataset(df):
    """
    Splitting the dataset to generate train an test dataset

    :type df: Pandas Dataframe
    :param df: Take a dataframe without blank cells in input
    :return: returns training and testing dataset
    """
    kf = KFold(n_splits=5, shuffle=True)
    result = next(kf.split(df), None)
    train = df.iloc[result[0]]
    test = df.iloc[result[1]]
    return train, test

def best_algo_prediction(algo, x_train, y_train, test):
    """

    :param algo:
    :param x_train:
    :param y_train:
    :param test:
    :return:
    """
    algo.fit(x_train, y_train)
    test_predictions = algo.predict_proba(test)
    return test_predictions

def export_predictions(csv=True, test_predictions=None, classes=None, test_ids=None, output_name=str()):
    """

    :param csv:
    :param test_predictions:
    :param classes:
    :param test_ids:
    :param output_name:
    :return:
    """
    if csv is True:
        # Format DataFrame
        submission = pd.DataFrame(test_predictions, columns=classes)
        submission.insert(0, 'id', test_ids)
        submission.reset_index()

        # Export Submission
        submission.to_csv(output_name + '.csv', index=False)
        submission.tail()

def binarize_y(y_test):
    """

    :param y_test:
    :return:
    """
    y_test = np.array([y_test])
    print(y_test)
    y_bin = preprocessing.Binarizer(threshold=0.5).transform(y_test)
    print(y_bin)
    print(np.mean(y_test != y_bin))
    return y_bin

def extract_labels(dataset):
    """

    :param dataset:
    :return:
    """
    possibilities = dataset.True_Label.unique()
    sub_dataset = dict()
    for option in possibilities:
        sub_dataset[option] = dataset.loc[dataset['True_Label'] == option]
    for k, v in sub_dataset.items():
        print(k, v)

def uniformize(x, y):
    """

    :param x:
    :param y:
    :return:
    """
    x_shape = x.shape[0]
    y_shape = y.shape[0]
    min_shape = min([x_shape, y_shape])
    return x.iloc[:min_shape], y.iloc[:min_shape]

def tuning_hyper_parameters(X, y, classifiers):
    for clf in classifiers:
        solver = ['newton-cg', 'lbfgs', 'liblinear', 'sag', 'saga']
        # print(param_grid)
        # tol = np.random.uniform(0, 1e-4, 100)
        param_grid = dict(solver=solver,
                          # tol=tol
                          )
        grid = GridSearchCV(clf, param_grid,
                            cv=3,
                            scoring='accuracy',
                            return_train_score=False,
                            n_jobs=-1
                            )
        grid.fit(X, y)
        print(pd.DataFrame(grid.cv_results_)[['mean_test_score', 'std_test_score', 'params']].to_string())
        print(grid.best_score_)
        print(grid.best_params_)
        exit()
        
def voting(classifiers):
    """

    Args:
        classifiers:

    Returns:

    """

    def warn(*args, **kwargs):
        pass

    warnings.warn = warn

    estimators = list()
    for clf in classifiers:
        estimators.append((clf.__class__.__name__, clf,))
    vot_clf = VotingClassifier(estimators=estimators,
                               voting="soft",
                               n_jobs=-1)
    return vot_clf


def process_predictor(logger,
                      predictor,
                      input_data,
                      output_dir,
                      train_or_test,
                      standardize,
                      fill_blanks,
                      strategy,
                      pd_dict,
                      data,
                      columns,
                      info):
    # output_dir = output_dir + "/" + predictor
    # mkdir(output_dir)
    # output_dir = output_dir + "/" + train_or_test
    # mkdir(output_dir)
    # output_dir = output_dir + "/Data_Description/"

    if type(predictor) is not list and predictor not in columns:
        logger.error('Predictor {} not in the Dataframe'.format(predictor))
        sys.exit("============\nSee you soon :)\n============")
    tmp_data = pd.DataFrame(data.loc[:, predictor])
    print('\n')
    logger.info('Data description of the predictor {}'.format(predictor))

    if type(predictor) is list:
        pred = "_".join(predictor)
    else:
        pred = predictor

    mkdir(output_dir + "/" + pred + "/")
    mkdir(output_dir + "/" + pred + "/" + train_or_test)
    mkdir(output_dir + "/" + pred + "/" + train_or_test + "/Data_Description/")

    write_to_html_file(tmp_data.describe(),
                       'Dataset Description - ' + pred + " - " + os.path.basename(input_data),
                       output_dir + "/" + pred + "/" + train_or_test + "/Data_Description/" + pred + '_dataset_description.html')
    tmp_data.describe().to_csv(
        output_dir + "/" + pred + "/" + train_or_test + "/Data_Description/" + pred + '_dataset_description.csv')
    print('\n')
    cols = tmp_data.columns

    tmp_data.reset_index()
    tmp_data.columns = [predictor]
    if fill_blanks is True:
        logger.info('Filling blanks ON for {}'.format(predictor))
        data_without_blanks = filling_blank_cells(tmp_data, strategy=strategy)
    if fill_blanks is False:
        logger.info('Filling blanks OFF for {}'.format(predictor))
        data_without_blanks = tmp_data.dropna()
    if standardize is True:
        logger.info('Standardization ON for {}'.format(predictor))
        data_without_blanks = StandardScaler().fit_transform(data_without_blanks)
    if standardize is False:
        logger.info('Standardization OFF for {}'.format(predictor))
        pass
    data_without_blanks = pd.DataFrame(data_without_blanks)
    data_without_blanks.columns = cols
    complete_data = merge_dataframe(info, data_without_blanks)
    # name = "|".join(predictor)
    if type(predictor) is list:
        predictor = "_".join(predictor)
    pd_dict[str(predictor)] = complete_data
    return pd_dict


# def test_R(self, df):
#     rprint = robjects.globalenv.get("print")
#     grdevices = importr('grDevices')
#     grdevices.png(file="Rpy2Curve.png", width=512, height=512)
#
#     VarClass_utils = rpackages.importr('factoextra')
#     lattice = rpackages.importr('lattice')
#
#     pandas2ri.activate()
#
#     data = pandas2ri.py2ri(df.iloc[:, 2:])
#
#     prcomp = robjects.r['prcomp']
#     fviz_eig = robjects.r['fviz_eig']
#     res_pca = prcomp(data, scale=True)
#     print(fviz_eig(res_pca))
#     grdevices.png(file="Rpy2Curve2.png", width=512, height=512)
#
#     fviz_pca_var = robjects.r['fviz_pca_var']
#     print(fviz_pca_var(res_pca))
#
#     grdevices.dev_off()
#
#     exit()
