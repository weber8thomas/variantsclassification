Core
~~~~~~

Training
==========

.. autoclass:: VarClass_scripts.training.TrainingClassification
    :members:

Testing
==========

.. autoclass:: VarClass_scripts.testing.TestingClassification
    :members:

