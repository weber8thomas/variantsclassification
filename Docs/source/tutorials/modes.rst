3. Modes
==========

There are three modes in VarClass
- Training mode
- Training and Testing mode
- Testing mode


Training mode
--------------------------

The aim of this mode is to TRAIN the different selected algorithms on selected data.

**Required parameters**
..........................

Input :

-i : input file of training data

**Optional parameters**
..........................

-fb

**Example**
..........................

Simple training without preprocessing with use of default parameters.

.. code-block:: python

    python VarClass.py --train \
                       -i Data/varibench_preprocess.csv.gz \


Training with preprocessing (filling blanks (--fill_blanks ; -fb) by using default strategy : "median" ; standardization (--standardize ; -std) of the data : µ = 0 ; std = 1).
Cross-validation parameter is enable with 5 fold (--cross_validation).
Plots (--plot) corresponding to ROC (Receiver Operating Curves)and Precision-Recall curves will be saved in the output directory (--output_dir) selected.
All the algorithms of classification will be compare (--full_clf) and parallelization will be enable (--threads = -1 = max value)

.. code-block:: python

    python VarClass.py --train \
                        --input Data/varibench_preprocess.csv.gz \
                        --output_dir Output_Test \
                        --full_clf \
                        --cross_validation 5 \
                        --fill_blanks \
                        --standardize \
                        --plot
                        --threads -1

Same command than before but here we want to compare only selected scenarios by splitting columns (--split_columns).
Predictors selected are : SIFT and PolyPhen2 alone and a third scenario with the two columns together.


.. code-block:: python

    python VarClass.py --train \
                       --input Data/varibench_preprocess.csv.gz \
                       --output_dir Output_Test \
                       --split_columns \
                       --predictors SIFT PolyPhen2 "SIFT;PolyPhen2" \
                       --full_clf \
                       --cross_validation 5 \
                       --fill_blanks \
                       --standardize \
                       --plot
                       --threads -1

Training and Testing mode
--------------------------


The aim of this mode is to TRAIN and TEST the different selected algorithms on selected data

**Required parameters**
..........................

Input :
-i : input file of training data

**Optional parameters**
..........................

-fb


Testing mode
--------------------------

The aim of this mode is to TEST the different selected algorithms on selected file by using models generated on similar data.

**Required parameters**
..........................

Input :
-i : input file of training data

**Optional parameters**
..........................

-fb
