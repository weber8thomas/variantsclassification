.. VariantsClassification documentation master file, created by
   sphinx-quickstart on Fri Nov 23 14:05:59 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to VarClass's documentation!
#####################################


VarClass is a small python program which is design to help user to train and test different machine learning algorithms on given data.


Contents :
----------

.. toctree::
   :maxdepth: 1

   tutorials/index
   documentation/index


Installation
-------------
To install VarClass, just clone the git repository and install the requirements cited below.

.. code-block:: bash

   git clone https://bitbucket.org/weber8thomas/variantsclassification.git



Requirements :
--------------

VarClass works on Python >= 3 and requires the following packages :

- matplotlib
- numpy
- pandas
- pathlib
- pprint
- scikit-learn>=0.20.0
- scipy
- seaborn

.. note::
   The Training and Testing dataset must have :

   - An "ID" column
   - A "True_Label" column
   - As many parameters as you want (";" and spaces in column headers are deprecated)

   Training and Testing dataset must also have the exact same structure (same number of parameters, same headers name)

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`