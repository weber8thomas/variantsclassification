import collections
from cyvcf2 import VCF, Writer
import sys
from pprint import pprint
from tqdm import tqdm

vcf = VCF(sys.argv[1])
w = Writer(sys.argv[2], vcf)

i=0
c=0
r=0
tmp_l = list()
for record in tqdm(vcf):
    r+=1
    csq = record.INFO.get('CSQ').split(',')
    for case in csq:
        record.INFO['CSQ'] = case
        split_case = case.split('|')
        tmp_l.append(split_case[1])
        # if split_case[1] == 'missense_variant':
        try:
            w.write_record(record)
            c += 1
        except:
            i+=1
print(i)
print(c)
print(r)
pprint(collections.Counter(tmp_l))