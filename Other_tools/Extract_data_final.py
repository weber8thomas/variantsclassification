from pprint import pprint
from tqdm import tqdm
import argparse
import collections
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import plotly
import plotly.figure_factory as ff
import plotly.graph_objs as go
import plotly.io as pio
import plotly.tools as tls
import sys, os
from itertools import cycle
import seaborn as sns
sns.set(style="darkgrid")
plt.style.use('ggplot')

plotly.io.orca.config.executable = "/home/weber/anaconda3/bin/orca"
plotly.io.orca.config.save()


def extract_data(dir, columns_interest, nrows=None, ncols=None):
    """

    Args:
        fname:str
            File name
        columns_interest:list
        plot:bool
        save:
        disp:

    Returns:

    """
    specific = ["SIFT", "PolyPhen", "CAROL", "Condel"]
    dict_infos = collections.defaultdict(list)
    dict_stats = collections.defaultdict(dict)

    final_dict = collections.defaultdict(dict)
    blank_dict = {
        "path" : np.array([]),
        "begn" : np.array([]),
        "VUS" : np.array([]) 
    }

    i = 0
    list_dir = os.listdir(dir)
    df_dict = collections.defaultdict()
    for data in list_dir:
        df = pd.read_csv(dir + '/' + data, sep='\t', compression='gzip', low_memory=False)
        n = os.path.basename(data).split('.')[0].split('.')[0]
        cols = ['ID', 'True_Label'] + [col + '_' + n for col in df.columns if col != ('ID') if col != ('True_Label')]
        df.columns = cols
        df_dict[n] = df

    df_dict = collections.OrderedDict(sorted(df_dict.items(), key=lambda t: t[0]))




    fig, axs = plt.subplots(nrows=nrows, figsize=(20,40))


    for i, c in enumerate(columns_interest):
        colors_path = ['#ff7979', '#eb4d4b']
        cycle_colors_path = cycle(colors_path)
        colors_begnin = ['#4834d4', '#30336b', '#22a6b3']
        cycle_colors_begnin = cycle(colors_begnin)
        colors_VUS = ['#e056fd'] 
        cycle_colors_VUS = cycle(colors_VUS)
        hist_data = list()
        group_labels = list()
        group_legend = list()
        colors = list()
        full_df = pd.DataFrame()
        for DB, dataframe in df_dict.items():
            col = c + '_' + DB
            tmp_df0 = dataframe[['True_Label', col]]
            len_raw = len(tmp_df0)
            tmp_df = tmp_df0.dropna()
            len_wt_na = len(tmp_df)
            pc_dropped = ((len_raw - len_wt_na)/len_raw) * 100
            tmp_df[col] = tmp_df.apply(lambda L: float(L[col]), axis=1)
            tmp_values_path = tmp_df.loc[tmp_df['True_Label'] == 1][col]
            sns.distplot(tmp_values_path, color=next(cycle_colors_path), label='Path-' + DB, hist=False, ax=axs[i])
            tmp_values_begn = tmp_df.loc[tmp_df['True_Label'] == -1][col]
            sns.distplot(tmp_values_begn, color=next(cycle_colors_begnin), label='Begn-' + DB, hist=False, ax=axs[i])
            tmp_values_vus = tmp_df.loc[tmp_df['True_Label'] == 0][col]
            if "_score" in c:
                tmp_c = c.split('_score')[0]
            if c == 'LogisticRegression_proba':
                tmp_c = 'VarScrut'
            else:
                tmp_c = c
                
            tmp_values_vus.name = tmp_c
            print(tmp_values_vus)
            sns.distplot(tmp_values_vus, color=next(cycle_colors_VUS), label='VUS', hist=False, ax=axs[i])
            # axs[i].set_title(c)

    plt.savefig('Test_seaborn.jpg')
            # exit()
        #     print(DB)
        #     print(len(tmp_values_path))
        #     print(len(tmp_values_begn))
        #     print(len(tmp_values_vus))
        #     if len(tmp_values_path) > 0:
        #         path = np.array(sorted(tmp_values_path.values))
        #         hist_data.append(path)
        #         group_labels.append('{}-Pathogenic : {}'.format(DB, str(len(path))))
        #         colors.append(next(cycle_colors_path))
        #         group_legend.append('group_path')
        #     if len(tmp_values_begn) > 0:
        #         begn = np.array(sorted(tmp_values_begn.values))
        #         hist_data.append(begn)
        #         group_labels.append('{}-Benign : {}'.format(DB, str(len(begn))))
        #         colors.append(next(cycle_colors_begnin))
        #         group_legend.append('group_begn')
        #     if len(tmp_values_vus) > 0:
        #         vus = np.array(sorted(tmp_values_vus.values))
        #         hist_data.append(vus)
        #         group_labels.append('VUS : {}'.format(str(len(vus))))
        #         colors.append(next(cycle_colors_VUS))
        #         group_legend.append('group_vus')

        # print(group_labels)

        # fig = ff.create_distplot(hist_data, group_labels, bin_size=.001, colors=colors, show_show_hist=False, hist=False, ax=axs[i])
        # fig['layout'].update(
        #     title='Distplot of {} databases'.format(
        #         str(', '.join(list(df_dict.keys())))))
        # fig['layout']['xaxis'].update(title='Score')
        # fig['layout']['yaxis'].update(title='Count (x100)')
        # fig['layout']['legend'].update(traceorder='normal')
        # # fig['layout']['legend'].update(x=0.1)
        # # fig['layout']['legend'].update(y=0.0)

        # pio.write_image(fig, output + '_' + c + '_' + str('_'.join(list(df_dict.keys()))) + '.jpg', format='jpg', width=1920, height=1080)

    return(final_dict.items())


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Extract DATA from CSV file', usage='%(prog)s [-h] [-i INI]')

    required = parser.add_argument_group('required arguments')

    #Args en input
    required.add_argument('-f', '--file_name',
                          metavar='',
                          type=str,
                          required=True,
                          help='Name of the csv file')


    parser.add_argument('-c', '--columns_of_interest',
                          metavar='',
                          nargs="*",
                          type=str,
                          help='Which columns of the CSV file you want to extract and plot')

    parser.add_argument('--nrows',
                          metavar='',
                          type=int,
                          help='Which columns of the CSV file you want to extract and plot')

    parser.add_argument('--ncols',
                          metavar='',
                          type=int,
                          help='Which columns of the CSV file you want to extract and plot')



    args = parser.parse_args()

    arg_dict = vars(args)

    final = extract_data(dir=arg_dict['file_name'],
                         columns_interest=arg_dict['columns_of_interest'],
                         nrows=arg_dict['nrows'],
                         ncols=arg_dict['ncols'],
                        )