from cyvcf2 import VCF
from pprint import pprint
from tqdm import tqdm
import argparse
import collections
import numpy as np
import os
import pandas as pd
import re
import sys

dict_consequence = {
    "transcript_ablation": 0.98,
    "frameshift_variant": 0.95,
    "splice_acceptor_variant": 0.9,
    "splice_donor_variant": 0.9,
    "stop_gained": 0.9,
    "stop_lost": 0.75,
    "transcript_amplification": 0.7,
    "inframe_insertion": 0.6,
    "inframe_deletion": 0.6,
    "missense_variant": 0.6,
    "tfbs_ablation": 0.6,
    "regulatory_region_ablation": 0.5,
    "start_lost": 0.4,
    "splice_region_variant": 0.3,
    "incomplete_terminal_codon_variant": 0.3,
    "stop_retained_variant": 0.3,
    "synonymous_variant": 0.25,
    "tfbs_amplification": 0.2,
    "tf_binding_site": 0.2,
    "regulatory_region_variant": 0.2,
    "NMD_transcript_variant": 0.15,
    "non_coding_transcript_variant": 0.15,
    "regulatory_region_amplification": 0.15,
    "mature_miRNA_variant": 0.1,
    "5_prime_UTR_variant": 0.1,
    "3_prime_UTR_variant": 0.1,
    "non_coding_transcript_exon_variant": 0.1,
    "intron_variant": 0.1,
    "upstream_gene_variant": 0.1,
    "downstream_gene_variant": 0.1,
    "feature_elongation": 0.1,
    "feature_truncation": 0.1,
    "intergenic_variant": 0.1,
}

thresholds_less = {
    'SIFT' : 0.05
}

thresholds_sup = {
    'PolyPhen' : 0.5,
    'CADD_PHRED' : 15,
    'MutationAssessor_score' : 0.65,
    'MutationTaster_score' : 0.5,
    'CAROL' : 0.98,
    'Condel' : 0.49,
    'VEST3_score' : 0.5,
    'SiPhy_29way_logOdds': 12.17,
    # 'phastCons100way_vertebrate' : '',
    # 'phastCons46way_placental' : '',
    # 'phastCons46way_primate' : '>0.7',
    # 'phyloP100way_vertebrate' : '',
    # 'phyloP46way_placental' : '',
    # 'phyloP46way_primate' : '',
    'REVEL' : 0.5,
    'GERP' : 4.4,
    # 'PHASTCONS' : '',
    'PHYLOP' : 1.6,
    # 'FITCONS' : '',
}

dict_convert = {
    'FATHMM_pred' :
        {
            'D' : 1,
            'T' : -1
        },
    'MetaLR_pred' :
        {
            'D' : 1,
            'T' : -1
        },
    'MetaSVM_pred' :
        {
            'D' : 1,
            'T' : -1
        },
    'LRT_pred' :
        {
            'D' : 1,
            'N' : -1,
            'U' : np.nan
        },
}


def parse(fname, output, columns_interest=None, true_label=None):
    """

    Args:
        fname:
        columns_interest:
        disp:

    Returns:

    """
    tmp_info = dict()
    dir = os.path.dirname(fname) + '/'
    basename = os.path.splitext(os.path.splitext(os.path.basename(fname))[0])[0]
    v = VCF(fname)

    for h in v.header_iter():
        try:
            if h.info()['ID'] == 'CSQ':
                csq_header = h.info()['Description'].split('|')
                index_list = list()
                for elem in columns_interest:
                    index_list.append(csq_header.index(elem))
                # print(csq_header)
                # exit()
        except:
            pass
    l = list()
    check_list = ['SIFT', 'PolyPhen', 'Condel', 'CAROL']

    i = 0
    for record in tqdm(v):
        tmp_dict = dict()
        id = str(record.CHROM) + '_' + str(record.POS) + '_' + str(record.REF) + '_' + str(record.ALT[0])
        tmp_dict['ID'] = id
        if true_label:
            tmp_dict['True_Label'] = true_label
        else:
            tmp_dict['True_Label'] = record.INFO.get('True_Label')
        if record.INFO.get('MCAP_pred'):
            tmp_dict['MCAP_pred'] = record.INFO.get('MCAP_pred')
        if record.INFO.get('Condel'):
            tmp_dict['Condel'] = record.INFO.get('Condel').split(',')[0]
        if record.INFO.get('CCRS_score'):
            tmp_dict['CCRS_score'] = float(record.INFO.get('CCRS_score'))
        if not record.INFO.get('CCRS_score'):
            tmp_dict['CCRS_score'] = 0
        info = record.INFO.get('CSQ').split('|')
        try:
            for i in index_list:
                value = info[i]
                if csq_header[i] in check_list:
                    try:
                        value = float(info[i][info[i].find("(") + 1:info[i].find(")")])
                    except:
                        value = np.nan
                # if str(csq_header[i]) == 'Consequence':
                #     try:
                #         value = value.split('&')
                #     except:
                #         pass
                #     consq = 0.0
                #     for elem in value:
                #         consq += dict_consequence[elem]
                #     consq = consq / len(value)
                #     value = consq
                if '&' in str(value) and '_pred' not in str(csq_header[i]) and str(csq_header[i]) != 'Consequence':
                    value = value.split('&')
                    value = [float(elem) for elem in value]
                    value = float(sum(value) / len(value))
                if '&' in str(value) and '_pred' in str(csq_header[i]):
                    value = value.split('&')
                    value = value[0]

                tmp_dict[csq_header[i]] = value

        except:
            i += 1
            pass
        l.append(tmp_dict)

    final_df = pd.DataFrame(data=l, columns=['ID'] + ['True_Label'] + columns_interest + ['MCAP_pred'] + ['CCRS_score'])

    # final_df.to_csv(path_or_buf=output, index=False, compression='gzip', sep='\t')
    # exit()

    list_of_str = ['SNV', 'A', 'T', 'C', 'G', 'U']
    # final_df = final_df[~final_df['CADD_RAW'].isin(list_of_str)]
    final_df = final_df[~final_df['CADD_PHRED'].isin(list_of_str)]
    # final_df = final_df[~final_df['LRT_pred'].isin(list_of_str)]
    # final_df = final_df.dropna()
    for col in final_df:
        col_name_pred = col + '_pred'
        if col in thresholds_less:
    
            final_df[col] = pd.to_numeric(final_df[col])
            final_df[col_name_pred] = np.where(final_df[col] < thresholds_less[col], '1', '-1')
        # try:

        if col in thresholds_sup:
            final_df[col] = pd.to_numeric(final_df[col])
            final_df[col_name_pred] = np.where(final_df[col] > thresholds_sup[col], '1', '-1')
        if col in dict_convert:
            final_df = final_df.replace({col : dict_convert[col]})
    
        # except:
        #     pass
    col_ordered = ['ID', 'True_Label'] + list(sorted(set(list(final_df.columns)) - set(['ID', 'True_Label'])))
    final_df = final_df[col_ordered]
    final_df = final_df.replace('', np.nan)
    na = final_df.isna().sum()
    print(final_df.isna().sum())
    na.to_csv('na.csv')
    # final_df = final_df.dropna()
    # final_df = final_df.drop_duplicates(subset='ID', keep=False)

    print(final_df)
    print('Pathogenic number : ' + str(len(final_df.loc[final_df['True_Label'] == '1'])))
    print('Begnin number : ' + str(len(final_df.loc[final_df['True_Label'] == '-1'])))
    #final_df.dropna(inplace=True)
    final_df.to_csv(path_or_buf=output, index=False, compression='gzip', sep='\t')


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='From VCF to CSV', usage='%(prog)s [-h] [-i INI]')

    required = parser.add_argument_group('required arguments')

    required.add_argument('-f', '--file_name',
                          metavar='',
                          type=str,
                          required=True,
                          help='Name of the VCF file')

    required.add_argument('-o', '--output_name',
                        type=str,
                        required=True,
                        help='Name of the output pandas file')

    parser.add_argument('-c', '--columns_of_interest',
                        metavar='',
                        nargs="*",
                        type=str,
                        help='Which values of INFO.CSQ features you want to extract')

    parser.add_argument('-l', '--label',
                        metavar='',
                        type=str,
                        help='Label')

    args = parser.parse_args()
    arg_dict = vars(args)
    final = parse(fname=arg_dict['file_name'],
                output=arg_dict['output_name'],
                  columns_interest=arg_dict['columns_of_interest'],
                  true_label=arg_dict['label']
                  )
