import pandas as pd
import sys

df = pd.read_csv(sys.argv[1], header=None)
df['True_Label'] = sys.argv[2]
df.to_csv(sys.argv[3], sep='\t', index=False)
