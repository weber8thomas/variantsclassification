from cyvcf2 import VCF, Writer
from tqdm import tqdm

v = VCF("exac_vep_vcfanno.vcf.gz")

line = {"ID" : "AC_Adj0_Filter",
        "Description" : 'Test'
        }
v.add_filter_to_header(line)
w = Writer("exac_vep_vcfanno_extract_CADD.vcf.gz", v)
# w.close()
# exit()
for h in v.header_iter():
    # print(h.info())
    # print(type(h.info()))
    try:
        if h.info()['ID'] == 'CSQ':
            csq_header = h.info()['Description'].split('|')
            index = csq_header.index('CADD_RAW')
            # print(len(csq_header))
    except:
        pass

# exit()
# exit()
# plugin = 0.0
total = 0
total_case = 0
for variant in tqdm(v):
    tmp = 0
    # case_nb = 0
    total += 1
    csq = variant.INFO.get('CSQ').split(',')
    # print(csq)
    for case in csq:
        # total_case += 1
        # case_nb += 1
        case = case.split('|')
        # print(case)
        # print()
        # print(case[index] + "is empty")
        if case[index]:

            # print(variant.ID, variant.CHROM, variant.POS, case[index])
            tmp += 1
    if tmp != 0:
        pass

    if tmp == 0:
        # print(variant)
        # exit()
        total_case += 1
        # print(variant)
        # print(total)
        # print(total_case)
        w.write_record(variant)

w.close()
v.close()
    # plugin += round(tmp/case_nb,2)
# print(plugin)
print(total)
print(total_case)
# print(round(float(plugin/total), 2))
    # print(csq)
    # exit()

# TODO : add FILTER AC_Adj0_Filter in the header
