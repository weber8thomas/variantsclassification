from cyvcf2 import VCF, Writer
import pandas as pd
import sys, os
from tqdm import tqdm
from pprint import pprint
import collections

dir_query = sys.argv[1]
dir_target = sys.argv[2]
list_dir_query = os.listdir(dir_query)
list_dir_target = os.listdir(dir_target)
dict_list = list()

for fq in tqdm(list_dir_query):
    if fq.endswith('.gz'):
        print(fq)
        tmp_list_q = list()
        vcf_query = VCF(dir_query + '/' + fq)
        vcf_o = Writer(dir_query + '/filter_' + fq, vcf_query)
        for record in tqdm(vcf_query, 'Parsing records in query...'):
            tmp_rec = str(record.CHROM) + '_' + str(record.POS) + '_' + str(record.REF) + '_' + str(record.ALT[0])
            tmp_list_q.append(tmp_rec)
        tmp_list_q =set(tmp_list_q)
        full_list = list()
        for ft in tqdm(list_dir_target):
            if ft.endswith('.gz'):
                print(ft)
                tmp_list_t = list()
                tmp_dict = dict()
                path = dir_target + '/' + ft
                vcf_target = VCF(path)
                for record in tqdm(vcf_target, desc='Parsing records in target ...'):
                    tmp_rec = str(record.CHROM) + '_' + str(record.POS) + '_' + str(record.REF) + '_' + str(record.ALT[0])
                    tmp_list_t.append(tmp_rec)
                    full_list.append(tmp_rec)
                tmp_list_t = set(tmp_list_t)
                intersec_l = tmp_list_q.intersection(tmp_list_t)
                intersection = len(intersec_l)
                print('{} - {} : {} common variants'.format(fq, ft, intersection))
                tmp_dict['Query_file'] = str(fq)
                tmp_dict['Target_file'] = str(ft)
                tmp_dict['Common_variants'] = intersection
                tmp_dict['Specific_query'] = str(len(tmp_list_q) - intersection)
                tmp_dict['Specific_target'] = str(len(tmp_list_t) - intersection)
                dict_list.append(tmp_dict)
        full_list = set(full_list)
        intersect_full = list(tmp_list_q.intersection(full_list))
        vcf_query = VCF(dir_query + '/' + fq)
        for record in tqdm(vcf_query, 'Writing filtered records...'):
            tmp_rec = str(record.CHROM) + '_' + str(record.POS) + '_' + str(record.REF) + '_' + str(record.ALT[0])
            if tmp_rec in intersect_full:
                pass
            else:
                vcf_o.write_record(record)

df = pd.DataFrame(data=dict_list)
df = df[["Query_file", "Target_file", "Common_variants", "Specific_query", "Specific_target"]]
df.to_csv(path_or_buf=dir_query + '/Comparison_table.csv',
          sep='\t',
          index=False)

# fq = sys.argv[1]
# dir_query = '/gstock/biolo_datasets/variation/benchmark/MODEL/VCF'


# for fq in tqdm(list_dir_query):
#     if fq.endswith('.gz'):
#         print(fq)
#         tmp_list_q = list()
#         vcf_query = VCF(dir_query + '/' + fq)
#         vcf_o = Writer(dir_query + '/filter_' + fq, vcf_query)
#         for record in tqdm(vcf_query, 'Parsing records in query...'):
#             tmp_rec = str(record.CHROM) + '_' + str(record.POS) + '_' + str(record.REF) + '_' + str(record.ALT[0])
#             tmp_list_q.append(tmp_rec)
#         tmp_list_q =set(tmp_list_q)
#         tmp_list_t = list()
#
#         for ft in tqdm(list_dir_target):
#             if ft.endswith('.gz'):
#                 tmp_dict = dict()
#                 path = dir_target + '/' + ft
#                 vcf_target = VCF(path)
#                 for record in tqdm(vcf_target, desc='Parsing records in target ...'):
#                     tmp_rec = str(record.CHROM) + '_' + str(record.POS) + '_' + str(record.REF) + '_' + str(record.ALT[0])
#                     tmp_list_t.append(tmp_rec)
#         tmp_list_t = set(tmp_list_t)
#         intersec_l = list(tmp_list_q.intersection(tmp_list_t))
#         vcf_query = VCF(dir_query + '/' + fq)
#         for record in tqdm(vcf_query, 'Writing filtered records...'):
#             tmp_rec = str(record.CHROM) + '_' + str(record.POS) + '_' + str(record.REF) + '_' + str(record.ALT[0])
#             if tmp_rec in intersec_l:
#                 pass
#             else:
#                 vcf_o.write_record(record)
