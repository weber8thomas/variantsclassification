from itertools import cycle
from pprint import pprint
from tqdm import tqdm
import argparse
import collections
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import plotly
import plotly.figure_factory as ff
import plotly.graph_objs as go
import plotly.io as pio
import plotly.tools as tls
import seaborn as sns
import sys, os

df1 = pd.read_csv(sys.argv[1], compression='gzip', sep='\t', low_memory=False)
df2 = pd.read_csv(sys.argv[2], compression='gzip', sep='\t', low_memory=False)

print(df1)
print(df2)