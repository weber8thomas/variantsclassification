from cyvcf2 import VCF, Writer
from pprint import pprint
from tqdm import tqdm
import argparse
import collections
import numpy as np
import os
import pandas as pd
import re
import sys

vcf_clinvar = VCF(sys.argv[1])
vcf_hgmd = VCF(sys.argv[2])
output_clinvar = Writer(sys.argv[3], vcf_clinvar)
output_hgmd = Writer(sys.argv[4], vcf_hgmd)

convert_dict = {
	'Pathogenic' : 1,
	'Likely pathogenic' : 1,
	'Pathogenic/Likely pathogenic' : 1,
	'Likely benign' : -1,
	'Benign' : -1,
	'Benign/Likely benign' : -1,
	'DM' : 1,
	'DM?' : 1,
	'DP' : -1,
	'FP' : -1,
	'DFP' : -1,
	'Uncertain_significance' : 0,
}

effect = list()
clinvar_list = list()
for record in tqdm(vcf_clinvar):
    id = str(record.CHROM) + '_' + str(record.POS) + '_' + str(record.REF) + '_' + str(record.ALT[0])
    csq = record.INFO.get('CSQ').split(',')
    tmp_missense = 0
    for case in csq:
        split_case = case.split('|')
        if (len(record.REF) and len(record.ALT[0])) == 1:
            if split_case[1] == 'missense_variant':
                tmp_missense += 1
                # print(record)
                # exit()
    if (tmp_missense) != 0:
    	effect.append(record.INFO.get('CLNSIG'))
    	if record.INFO.get('CLNSIG') and record.INFO.get('CLNSIG') in convert_dict:
        	clinvar_list.append({'ID' : id, 'ClinSig_Clinvar' : convert_dict[record.INFO.get('CLNSIG')]})
clinvar_df = pd.DataFrame(clinvar_list)

hgmd_list = list()
for record in tqdm(vcf_hgmd):
    id = str(record.CHROM) + '_' + str(record.POS) + '_' + str(record.REF) + '_' + str(record.ALT[0])
    csq = record.INFO.get('CSQ').split(',')
    tmp_missense = 0
    for case in csq:
        split_case = case.split('|')
        if (len(record.REF) and len(record.ALT[0])) == 1:
            if split_case[1] == 'missense_variant':
                tmp_missense += 1
                # print(record)
                # exit()
    if (tmp_missense) != 0:
        if record.INFO.get('CLASS') and record.INFO.get('CLASS') in convert_dict:
        	hgmd_list.append({'ID' : id, 'ClinSig_HGMD' : convert_dict[record.INFO.get('CLASS')]})
hgmd_df = pd.DataFrame(hgmd_list)
clinvar_df = clinvar_df.drop_duplicates('ID')
clinvar_df = clinvar_df.set_index('ID')
hgmd_df = hgmd_df.drop_duplicates('ID')
hgmd_df = hgmd_df.set_index('ID')
# clinvar_df = clinvar_df.reset_index()
# hgmd_df =  hgmd_df.reset_index()
merge_df = pd.concat([clinvar_df, hgmd_df], axis=1)
vus = merge_df.loc[merge_df['ClinSig_Clinvar'] == 0]
merge_df_wt_na = merge_df.dropna()
conflicting = merge_df_wt_na.loc[merge_df_wt_na['ClinSig_Clinvar'] != merge_df_wt_na['ClinSig_HGMD']]
correct = merge_df_wt_na.loc[merge_df_wt_na['ClinSig_Clinvar'] == merge_df_wt_na['ClinSig_HGMD']]
final_list = list(vus.index.values) + list(conflicting.index.values)
final_list = sorted(list(set(final_list)))
correct_list = sorted(list(set(correct.index.values)))
vcf_clinvar = VCF(sys.argv[1])
vcf_hgmd = VCF(sys.argv[2])


for record in tqdm(vcf_clinvar):
    id = str(record.CHROM) + '_' + str(record.POS) + '_' + str(record.REF) + '_' + str(record.ALT[0])
    if id in final_list and id not in correct_list:
    	output_clinvar.write_record(record)

for record in tqdm(vcf_hgmd):
	id = str(record.CHROM) + '_' + str(record.POS) + '_' + str(record.REF) + '_' + str(record.ALT[0])
	if id in final_list and id not in correct_list:
		output_hgmd.write_record(record)