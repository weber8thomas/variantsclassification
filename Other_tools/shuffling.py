import sys
import collections
from pprint import pprint
import pandas as pd
import argparse
from cyvcf2 import VCF
import os
from tqdm import tqdm
import numpy as np

dir_path = sys.argv[1]
dir = os.listdir(dir_path)
tmp = pd.read_csv(filepath_or_buffer=dir_path + '/' + dir[0],
                         sep='\t',
                         compression='gzip',
                         encoding='utf-8')
final_df = pd.DataFrame(columns=tmp.columns)


thresholds_less = {
    'SIFT' : 0.05
}

thresholds_sup = {
    'PolyPhen' : 0.5,
    'CADD_PHRED' : 15,
    'MutationAssessor_score' : 0.65,
    'MutationTaster_score' : 0.5,
    'CAROL' : 0.98,
    'Condel' : 0.49,
    'VEST3_score' : 0.5,
    # 'SiPhy_29way_logOdds': 12.17,
    # 'phastCons100way_vertebrate' : '',
    # 'phastCons46way_placental' : '',
    # 'phastCons46way_primate' : '>0.7',
    # 'phyloP100way_vertebrate' : '',
    # 'phyloP46way_placental' : '',
    # 'phyloP46way_primate' : '',
    'REVEL' : 0.5,
    # 'GERP' : 4.4,
    # 'PHASTCONS' : '',
    # 'PHYLOP' : 1.6,
    # 'FITCONS' : '',
}

dict_convert = {
    'FATHMM_pred' :
        {
            'D' : 1,
            'T' : -1
        },
    'MetaLR_pred' :
        {
            'D' : 1,
            'T' : -1
        },
    'MetaSVM_pred' :
        {
            'D' : 1,
            'T' : -1
        },
    # 'LRT_pred' :
    #     {
    #         'D' : 1,
    #         'N' : -1
    #     },
}


for file in dir:
    print(file)
    tmp_df = pd.read_csv(filepath_or_buffer=dir_path + '/' + file,
                         sep='\t',
                         compression='gzip',
                         encoding='utf-8',
                         low_memory=False
                         )
    # sample = tmp_df.sample(n=10000)
    # train_df = train_df.append(sample.head(n=8000), ignore_index=True)
    # test_df = test_df.append(sample.tail(n=2000), ignore_index=True)
    final_df = final_df.append(tmp_df)
list_of_str = ['SNV', 'A', 'T', 'C', 'G', 'U']
# final_df = final_df[~final_df['CADD_RAW'].isin(list_of_str)]
# final_df = final_df[~final_df['CADD_PHRED'].isin(list_of_str)]
# final_df = final_df[~final_df['LRT_pred'].isin(list_of_str)]
# final_df = final_df.dropna()

# for col in final_df:
#     col_name_pred = col + '_pred'
#     if col in thresholds_less:

#         final_df[col] = pd.to_numeric(final_df[col])
#         final_df[col_name_pred] = np.where(final_df[col] < thresholds_less[col], '1', '-1')
#     # try:
#     if col in thresholds_sup:
#         final_df[col] = pd.to_numeric(final_df[col])
#         final_df[col_name_pred] = np.where(final_df[col] > thresholds_sup[col], '1', '-1')
#     if col in dict_convert:
#         final_df = final_df.replace({col : dict_convert[col]})

    # except:
    #     pass
col_ordered = ['ID', 'True_Label'] + list(sorted(set(list(final_df.columns)) - set(['ID', 'True_Label'])))
final_df = final_df[col_ordered]
print(final_df.isna().sum())
final_df = final_df.dropna()
final_df = final_df.drop_duplicates(subset='ID', keep='first')
print(final_df)
print('Pathogenic number : ' + str(len(final_df.loc[final_df['True_Label'] == 1])))
print('Begnin number : ' + str(len(final_df.loc[final_df['True_Label'] == -1])))
final_df.to_csv(path_or_buf=dir_path + '/complete_data.csv.gz',
                sep='\t',
                compression='gzip',
                encoding='utf-8',
                index=False,
                )
