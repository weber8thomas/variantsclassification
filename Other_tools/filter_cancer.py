from cyvcf2 import VCF, Writer
from pprint import pprint
from tqdm import tqdm
import argparse
import collections
import numpy as np
import os
import pandas as pd
import re
import sys

cancer_file = sys.argv[1]
vcf = VCF(sys.argv[2])
o = Writer(sys.argv[3], vcf)

cancer_list = list()

with open(cancer_file, 'r') as cl:
	for cancer_gene in cl:
		cancer_list.append(cancer_gene.strip())

full_path = 0
full_begn = 0
wt_path = 0
wt_begn = 0

for record in tqdm(vcf):
	tl = int(record.INFO.get('True_Label'))
	if tl == 1:
		full_path +=1
	if tl == -1:
		full_begn +=1
	csq = record.INFO.get('CSQ').split(',')
	cancer_check = 0
	for case in csq:
		case = case.split('|')
		if case[3] in cancer_list:
			cancer_check += 1
	if cancer_check == 0:
		o.write_record(record)
		if tl == 1:
			wt_path +=1
		if tl == -1:
			wt_begn +=1
	else:
		pass

print(str('Full path : {}').format(full_path))
print(str('Full begn : {}').format(full_begn))
print(str('Without cancer path : {}').format(wt_path))
print(str('Without cancer begn path : {}').format(wt_begn))