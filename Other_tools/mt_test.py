import numpy as np
import re
import sys
import collections
from pprint import pprint
import pandas as pd
import argparse
from cyvcf2 import VCF, Writer
import os
from tqdm import tqdm
import plotly
import plotly.graph_objs as go
import plotly.figure_factory as ff
import plotly.io as pio

plotly.io.orca.config.executable = "/home/weber/anaconda3/bin/orca"
plotly.io.orca.config.save()

v = VCF(sys.argv[1])

for h in v.header_iter():
    try:
        if h.info()['ID'] == 'CSQ':
            csq_header = h.info()['Description'].split('|')
            index_list = list()
            for elem in columns_interest:
                index_list.append(csq_header.index(elem))
            # print(csq_header)
            # exit()
    except:
        pass

check_l = ["MutationTaster_score", "MutationTaster_pred"]
compare_dict_vep = collections.defaultdict(dict)
compare_dict_vcfanno = collections.defaultdict(dict)

for record in tqdm(v):
    id = str(record.CHROM) + '_' + str(record.POS) + '_' + str(record.REF) + '_' + str(record.ALT[0])
    csq = record.INFO.get('CSQ').split(',')
    for case in csq:
        split_case = case.split('|')
        for check in check_l:
            compare_dict_vep[id][check] = split_case[csq_header.index(check)]
    for check in check_l:
        if record.INFO.get(check) != None:
            value = str(record.INFO.get(check))
            if value == ".":
                value = np.nan
            if check == check_l[0]:
                if ',' in str(record.INFO.get(check)):
                    tmp_check = record.INFO.get(check).split(',')
                    value = 0.0
                    value = sum([value + float(e) for e in tmp_check if '.' not in str(e)])/len(tmp_check)
                if ';' in str(record.INFO.get(check)):
                    tmp_check = record.INFO.get(check).split(';')
                    value = 0.0
                    value = sum([value + float(e) for e in tmp_check]) / len(tmp_check)

            compare_dict_vcfanno[id].update({check: value})
        elif record.INFO.get(check) == None :
            compare_dict_vcfanno[id].update({check: np.nan})

for k_vep, k_vcfanno in zip(compare_dict_vep.keys(), compare_dict_vcfanno.keys()):
    if k_vep == k_vcfanno:
        k = k_vep
        v_vep = compare_dict_vep[k][check_l[0]]
        if v_vep == '':
            v_vep = np.nan
        v_anno = compare_dict_vcfanno[k][check_l[0]]
        v_vep = float(v_vep)
        v_anno = float(v_anno)
        compare_dict_vep[k][check_l[0]] = v_vep
        compare_dict_vcfanno[k][check_l[0]] = v_anno


df_vep = pd.DataFrame(compare_dict_vep).T
df_vcfanno = pd.DataFrame(compare_dict_vcfanno).T
histo_df = pd.concat([df_vep["MutationTaster_score"], df_vcfanno["MutationTaster_score"]], axis = 1)
histo_df.columns = ["MutationTaster_score_VEP", "MutationTaster_score_VCFANNO"]
hist_data = []
group_labels = []
for col in histo_df.columns:
    values = np.array(sorted(list(histo_df[col].dropna().values)))
    hist_data.append(values)
    group_labels.append(col + '_' + str(len(values)))
fig = ff.create_distplot(hist_data, group_labels, bin_size=.001, show_hist=False,show_rug=False)
pio.write_image(fig, 'test' + '.jpg', format='jpg', width=1920, height=1080)

