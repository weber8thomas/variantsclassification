from IPython.display import IFrame
from pprint import pprint
import pandas as pd
import plotly
import plotly.graph_objs as go
import matplotlib.pyplot as plt

import plotly.io as pio
import random
import seaborn as sns
import sys, os
sns.set(style="darkgrid")
sns.palplot(sns.color_palette("Paired"))

plt.style.use('seaborn')
plotly.io.orca.config.executable = "/maison/sage2016/weber/anaconda3/bin/orca"
plotly.io.orca.config.save()

dir = sys.argv[1]
list_dir = list(sorted(os.listdir(dir)))
complete_df = pd.DataFrame()
complete_df_scores = pd.DataFrame()

order = ["GERP", "SiPhy", "LRT", "PolyPhen", "SIFT", "VEST3", "Condel", "CADD",
         "FATHMM", "MetaLR", "MetaSVM", "MCAP", "LogisticRegression" ]



db_order = ['HUMVAR', 'VARIBENCH', 'EXOVAR', 'PREDICTSNP', 'SWISSVAR', 'VARDATA']

group_colors = {
    "GERP": "#FFB74D",
    "PHYLOP": "#FFA726",
    "SiPhy": "#FF9800",

    "LRT": "#AED581",
    "MutationAssessor": "#9CCC65",
    "MutationTaster":   "#8BC34A",
    "PolyPhen": "#81C784",
    "SIFT":     "#66BB6A",
    "VEST3":    "#4CAF50",

    "CADD": "#90CAF9",
    "CAROL": "#64B5F6",
    "Condel": "#42A5F5",
    "FATHMM": "#2196F3",
    "MetaLR": "#1E88E5",
    "MetaSVM": "#1976D2",
    "MCAP": "#64B5F6",
    "VarScrut_GradBoost": "#AA00FF",
    "VarScrut_Logit": "#E91E63",
}

legend_group ={
    "GERP": "cons_group",
    "PHYLOP": "cons_group",
    "SiPhy": "cons_group",

    "LRT":  "predictor_group",
    "MutationAssessor": "predictor_group",
    "MutationTaster": "predictor_group",
    "PolyPhen": "predictor_group",
    "SIFT": "predictor_group",
    "VEST3": "predictor_group",

    "CADD": "meta predictor_group",
    "CAROL": "meta predictor_group",
    "Condel": "meta predictor_group",
    "FATHMM": "meta predictor_group",
    "MetaLR": "meta predictor_group",
    "MetaSVM": "meta predictor_group",
    "MCAP": "meta predictor_group",
    "VarScrut_GradBoost": "meta predictor_group",
    "VarScrut_Logit": "meta predictor_group",
}


convert_dicf = {
    'GradientBoostingClassifier' : 4,
    'LinearDiscriminantAnalysis' : 6,
    'LogisticRegression' : 7,
    'MLPClassifier' : 9,
    'RandomForestClassifier' : 13
}

sns.set(rc={'figure.figsize':(75, 25)})

for d in list_dir:
    df = pd.read_csv(dir + '/' + d + '/TEST/Reports/Classification_summary.csv',
                     sep='\t')
    df
    auc = df[['Classifier', 'AUC Score']]
    auc.columns = ['Classifier', d]


    # second_level_index = list(df.columns)
    # second_level_index.remove('Classifier')
    # first_level_index = [d for i in range(len(second_level_index))]
    # arrays = [first_level_index, second_level_index]
    # tmp_tuple = list(zip(*arrays))
    # full_df = df
    # print(full_df)
    auc = auc.set_index('Classifier')
    # print(full_df)
    # exit()
    # full_df.set_value(0, 'Classifier', 'VarScrut_Logit')
    # full_df = full_df.set_index('Classifier')
    # full_df = full_df.reindex(order).T
    # full_df.index = pd.MultiIndex.from_tuples(tmp_tuple)
    # complete_df_scores = pd.concat([complete_df_scores, full_df], axis=0)
    
    # auc.columns = [d]
    complete_df = pd.concat([complete_df, auc], axis=1)
complete_df = complete_df[db_order]
complete_df = complete_df.reindex(order)
complete_df['Classifier'] = complete_df.index
complete_df = pd.melt(complete_df, id_vars='Classifier', var_name="DB", value_name='AUC_Score')
g = sns.catplot(x='DB', y='AUC_Score', hue='Classifier', data=complete_df, kind='bar', height=10, aspect=20/10)

ax=g.ax #annotate axis = seaborn axis
def annotateBars(row, ax=ax): 
    for p in ax.patches:
         ax.annotate("%.2f" % p.get_height(), (p.get_x() + p.get_width() / 2., p.get_height()),
             ha='center', va='center', fontsize=11, color='gray', rotation=90, xytext=(0, 20),
             textcoords='offset points') 
output = sys.argv[2]
condition = sys.argv[3]
title = 'ROC AUC between different databases in condition : ' + str(condition) + ' with Model : ' + str(dir)

g.fig.suptitle(title)
plot = df.apply(annotateBars, ax=ax, axis=1)
plt.savefig(output + '.jpg', dpi=150)

# exit()
# for clf in complete_df:
#     serie = complete_df[clf]
#     print(serie)
#     sns.barplot(x=serie.index, y=serie)
#     plt.show()    
#     exit()

# exit()
# # print(complete_df_scores)
# # complete_df_scores.to_csv(dir + '/metrics_table.csv', sep = "\t")


# # complete_df['Classifier'] = df['Classifier']
# # complete_df.set_value(0, 'Classifier', 'VarScrut_Logit')
# # complete_df = complete_df.set_index('Classifier')
# # complete_df = complete_df.reindex(order)
# final_dict = complete_df.to_dict("index")
# y = list()
# for d in [final_dict[d] for d in final_dict]:
#     y.extend(list(d.keys()))
# # y = [k for k in final_dict[d] for d in final_dict]
# y = list(sorted(set(y)))
# print(y)
# exit()
# x = list(next(iter(final_dict.values())).keys())

# print(x)
# exit()
# # selected_color_palette = random.choice(list(dict_colors.values()))

# data = list()
# for col in final_dict:
#     rounded_values = list(final_dict[col].values())
#     rounded_values = [round(e, 2) for e in rounded_values]
#     trace = go.Bar(
#         x=x,
#         y=list(final_dict[col].values()),
#         text=rounded_values,
#         name=col,
#         # legendgroup=legend_group[col],
#         textposition='auto',
#         marker=dict(
#             # color=group_colors[col]
#         )
#     )
#     data.append(trace)

# layout = go.Layout(
#     xaxis=dict(
#         tickfont=dict(
#             size=20,
#             family='sans-serif'
#         ),

#     ),
#     yaxis=dict(
#         title='AUC',
#         titlefont=dict(
#             size=20,
#             family='sans-serif'
#         ),
#         range=[0, 1]


#     ),
#     barmode='group',

#     legend=dict(
#             font=dict(
#                 family='sans-serif',
#                 size=16,
#             ),
#         )
# )

# fig = go.Figure(data=data, layout=layout)
# pio.write_image(fig, sys.argv[2] + '.jpg', format='jpg', width=1920, height=1080)
