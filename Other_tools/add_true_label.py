import numpy as np
import re
import sys
import collections
from pprint import pprint
import pandas as pd
import argparse
from cyvcf2 import VCF, Writer
import os
from tqdm import tqdm

vcf = VCF(sys.argv[1])

label = str(sys.argv[2])

vcf.add_info_to_header({
    'ID' : 'True_Label',
    'Description' : 'True_Label of the variation',
    'Type': 'String',
    'Number' : '1',
})


w = Writer(sys.argv[3], vcf)


for record in tqdm(vcf):
    id = str(record.CHROM) + '_' + str(record.POS) + '_' + str(record.REF) + '_' + str(record.ALT[0])
    true_label = label
    record.INFO['True_Label'] = label
    w.write_record(record)