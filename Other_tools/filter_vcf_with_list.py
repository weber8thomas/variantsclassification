import re
import sys
import collections
from pprint import pprint
import pandas as pd
import argparse
from cyvcf2 import VCF, Writer
import os
from tqdm import tqdm

file = sys.argv[1]
l = sys.argv[2]
output = sys.argv[3]

list_positions = list()
with open(l) as l_file:
    for line in l_file:
        pos = line.strip()
        list_positions.append(pos)

vcf = VCF(file)
w = Writer(output, vcf)
for record in tqdm(vcf):
    id = str(record.CHROM) + '_' + str(record.POS) + '_' + str(record.REF) + '_' + str(record.ALT[0])
    if id in list_positions:
        pass
    else:
        w.write_record(record)
