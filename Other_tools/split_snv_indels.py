import numpy as np
import re
import sys
import collections
from pprint import pprint
import pandas as pd
import argparse
from cyvcf2 import VCF, Writer
import os
from tqdm import tqdm

vcf = VCF(sys.argv[1])
w_snv = Writer(sys.argv[2], vcf)
w_indels = Writer(sys.argv[3], vcf)

for record in vcf:
	if len(record.REF) == 1:
		w_snv.write_record(record)
	if len(record.REF) > 1:	
		w_indels.write_record(record)