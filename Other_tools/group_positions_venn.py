import pandas as pd
import sys, os
from rpy2.robjects.packages import importr
from rpy2 import robjects
from rpy2.robjects import pandas2ri
import subprocess


pandas2ri.activate()
grdevices = importr('grDevices')
base = importr('base')
utils = importr('utils')
dir = sys.argv[1]
list_dir = os.listdir(dir)
pd_dict = dict()
merge_df = pd.DataFrame()
for file in list_dir:
    df = pd.read_csv(dir + '/' + file,
                     header=None,
                     compression="gzip",
                     low_memory=False)
    basename = str(file.split('_positions')[0]) + ' (' + str(int(len(df))) + ')'
    df.columns = ['Positions']
    df['File'] = basename
    merge_df = merge_df.append(df, ignore_index=True)
merge_df = merge_df[['File', 'Positions']]
crstb = pd.crosstab(index=merge_df['Positions'], columns=[merge_df['File']])
crstb.to_csv('tmp_df.csv.gz', compression='gzip', sep='\t', index=False)
print('R script starting')
subprocess.call ("/biolo/R_ubuntu/R-3.4.4/bin/Rscript --vanilla /gstock/biolo_datasets/variation/benchmark/VarScrut_Final/Scripts/venn.R", shell=True)
