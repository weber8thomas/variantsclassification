import numpy as np
from cyvcf2 import Writer, VCF
from tqdm import tqdm
import pandas as pd
import re

# print(re.search('\(([^)]+)', test).group(1))
# print(test.split('(')[0])
# exit()

vcf = VCF("exac_vep_extract.vcf.gz")


basic = ["CHROM", "POS", "REF", "ALT", ]
soft = ['SIFT', 'PolyPhen', 'CADD_PHRED', 'CADD_RAW', 'CAROL', 'Condel', 'FATHMM_pred', 'FATHMM_score', 'LRT_pred', 'LRT_score',
        'MetaLR_pred', 'MetaLR_score', 'MetaSVM_pred', 'MetaSVM_score', 'MutationAssessor_pred',
        'MutationAssessor_score', 'MutationTaster_score', 'SiPhy_29way_logOdds', 'VEST3_score',
        'phastCons100way_vertebrate', 'phastCons46way_placental', 'phastCons46way_primate', 'phyloP100way_vertebrate',
        'phyloP46way_placental', 'phyloP46way_primate', 'Grantham', 'GERP', 'PHASTCONS',
        'PHYLOP', 'FITCONS', 'REVEL']

for h in vcf.header_iter():
    try:
        if h.info()['ID'] == 'CSQ':
            csq_header = h.info()['Description'].split('|')
            index = list()
            [index.append(csq_header.index(elem)) for elem in soft]
    except:
        pass

slice = list()
for record in tqdm(vcf):
    csq = record.INFO.get('CSQ').split(',')
    tmp_dict = dict()
    tmp_dict['CHROM'] = record.CHROM
    tmp_dict['POS'] = record.POS
    tmp_dict['REF'] = record.REF
    tmp_dict['ALT'] = record.ALT[0]
    for case in csq:
        tmp_case = tmp_dict
        tmp_list = list()
        case = case.split('|')
        {tmp_case.update({s : case[i]}) for i, s in zip(index, soft)}
        slice.append(tmp_case)
df = pd.DataFrame(slice).drop_duplicates().reset_index(drop=True)

process_col = ['SIFT', 'PolyPhen', 'CAROL', 'Condel']

for col in process_col:
    pred = col + '_pred'
    score = col + '_score'
    df[pred] = df[col].str.split('(').str[0].str.lower()
    df[score] = df[col].apply(lambda st: st[st.find('(')+1:st.find(')')])
    df2 = pd.concat([df[pred], df[score]], axis=1, sort=False)
    df = df.drop(col, 1)

columns = ['CHROM',
           'POS',
           'REF',
           'ALT',
           'CADD_PHRED',
           'CADD_RAW',
           'CAROL_pred',
           'CAROL_score',
           'Condel_pred',
           'Condel_score',
           'FATHMM_pred',
           'FATHMM_score',
           'FITCONS',
           'GERP',
           'Grantham',
           'LRT_pred',
           'LRT_score',
           'MetaLR_pred',
           'MetaLR_score',
           'MetaSVM_pred',
           'MetaSVM_score',
           'MutationAssessor_pred',
           'MutationAssessor_score',
           'MutationTaster_score',
           'PHASTCONS',
           'phastCons100way_vertebrate',
           'phastCons46way_placental',
           'phastCons46way_primate',
           'PHYLOP',
           'phyloP100way_vertebrate',
           'phyloP46way_placental',
           'phyloP46way_primate',
           'PolyPhen_pred',
           'PolyPhen_score',
           'REVEL',
           'SIFT_pred',
           'SIFT_score',
           'SiPhy_29way_logOdds',
           'VEST3_score']

df = df[columns]
df.replace('', np.nan, inplace=True)
df = df.dropna()


# print(df[['CHROM', 'POS', 'REF', 'SIFT_pred', 'SIFT_score']])
df.to_csv(path_or_buf="exac_test_matrix.csv.gz", index=False, compression='gzip')