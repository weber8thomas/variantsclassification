from cyvcf2 import VCF, Writer
# import vcf
from pprint import pprint
from tqdm import tqdm
import re


vcf = VCF("exac_vep_vcfanno.vcf.gz")

vcf.add_filter_to_header({'ID': 'AC_Adj0_Filter', 'Description' : 'Test'})

# w_001 = Writer("exac_vep_filter_001.vcf.gz", vcf)
# w_005 = Writer("exac_vep_filter_005.vcf.gz", vcf)

total_case = 0
total = 0
SNV = 0
missense = 0
rare = 0
polymorphic = 0

for record in tqdm(vcf):
    total += 1
    csq = record.INFO.get('CSQ').split('|')
    # tmp_snv = 0
    # tmp_missense = 0


    # for case in csq:
    #     total_case += 1
    #     split_case = case.split('|')

    if csq[22] == 'SNV' and csq[1] == 'missense_variant':
        SNV += 1

        # if split_case[1] == 'missense_variant':
        #     tmp_missense += 1

    # if (tmp_snv and tmp_missense) != 0:
    #     SNV += 1
    #     missense += 1

        if record.INFO.get('ESP_AF_GLOBAL') == 'NA':
            rare += 1
            # w_001.write_record(record)
        if record.INFO.get('ESP_AF_GLOBAL') and record.INFO.get('ESP_AF_GLOBAL') != 'NA':
            if float(record.INFO.get('ESP_AF_GLOBAL')) <= 0.01:
                rare += 1
                # w_001.write_record(record)
            if float(record.INFO.get('ESP_AF_GLOBAL')) >= 0.05:
                polymorphic += 1
                # w_005.write_record(record)

# print(total_case)
print(total)
print(SNV)
# print(missense)
print(rare)
print(polymorphic)
