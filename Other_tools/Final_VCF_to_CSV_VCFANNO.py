from cyvcf2 import VCF
from pprint import pprint
from tqdm import tqdm
import argparse
import collections
import numpy as np
import os
import pandas as pd
import re
import sys

dict_consequence = {
    "transcript_ablation": 0.98,
    "frameshift_variant": 0.95,
    "splice_acceptor_variant": 0.9,
    "splice_donor_variant": 0.9,
    "stop_gained": 0.9,
    "stop_lost": 0.75,
    "transcript_amplification": 0.7,
    "inframe_insertion": 0.6,
    "inframe_deletion": 0.6,
    "missense_variant": 0.6,
    "tfbs_ablation": 0.6,
    "regulatory_region_ablation": 0.5,
    "start_lost": 0.4,
    "splice_region_variant": 0.3,
    "incomplete_terminal_codon_variant": 0.3,
    "stop_retained_variant": 0.3,
    "synonymous_variant": 0.25,
    "tfbs_amplification": 0.2,
    "tf_binding_site": 0.2,
    "regulatory_region_variant": 0.2,
    "NMD_transcript_variant": 0.15,
    "non_coding_transcript_variant": 0.15,
    "regulatory_region_amplification": 0.15,
    "mature_miRNA_variant": 0.1,
    "5_prime_UTR_variant": 0.1,
    "3_prime_UTR_variant": 0.1,
    "non_coding_transcript_exon_variant": 0.1,
    "intron_variant": 0.1,
    "upstream_gene_variant": 0.1,
    "downstream_gene_variant": 0.1,
    "feature_elongation": 0.1,
    "feature_truncation": 0.1,
    "intergenic_variant": 0.1,
}

thresholds_less = {
    'SIFT' : 0.05
}

thresholds_sup = {
    'PolyPhen' : 0.5,
    'CADD_PHRED' : 15,
    'MutationAssessor_score' : 0.65,
    'MutationTaster_score' : 0.5,
    'CAROL' : 0.98,
    'Condel' : 0.49,
    'VEST3_score' : 0.5,
    'SiPhy_29way_logOdds': 12.17,
    # 'phastCons100way_vertebrate' : '',
    # 'phastCons46way_placental' : '',
    # 'phastCons46way_primate' : '>0.7',
    # 'phyloP100way_vertebrate' : '',
    # 'phyloP46way_placental' : '',
    # 'phyloP46way_primate' : '',
    'REVEL' : 0.5,
    'GERP' : 4.4,
    # 'PHASTCONS' : '',
    'PHYLOP' : 1.6,
    # 'FITCONS' : '',
}

dict_convert = {
    'FATHMM_pred' :
        {
            'D' : 1,
            'T' : -1
        },
    'MetaLR_pred' :
        {
            'D' : 1,
            'T' : -1
        },
    'MetaSVM_pred' :
        {
            'D' : 1,
            'T' : -1
        },
    'LRT_pred' :
        {
            'D' : 1,
            'N' : -1,
            'U' : np.nan,
        },
    'Polyphen2_HVAR_pred' : {
    'D' : 1,
    'P' : 1,
    'B' : 0,
    },
    'M-CAP_pred':{
    'T' : -1,
    'D' : 1
    },
    'SIFT4G_pred':{
    'T' : -1,
    'D' : 1
    }

}


def parse(fname, output, columns_interest=None, true_label=None):
    """

    Args:
        fname:
        columns_interest:
        disp:

    Returns:

    """
    tmp_info = dict()
    dir = os.path.dirname(fname) + '/'
    basename = os.path.splitext(os.path.splitext(os.path.basename(fname))[0])[0]
    v = VCF(fname)


    l = list()
    check_list = ['SIFT', 'PolyPhen', 'Condel', 'CAROL']

    print(columns_interest)

    dict_asso = dict()



    j=0
    e = 0
    for record in tqdm(v):
        tmp_dict = dict()
        index_dict = dict()
        id = str(record.CHROM) + '_' + str(record.POS) + '_' + str(record.REF) + '_' + str(record.ALT[0])
        # print(id)
        tmp_dict['ID'] = id
        tmp_dict['True_Label'] = record.INFO.get('True_Label')
        if record.INFO.get('CCRS_score'):
            tmp_dict['CCRS_score'] = float(record.INFO.get('CCRS_score'))
        if not record.INFO.get('CCRS_score'):
            tmp_dict['CCRS_score'] = 0
        for col in columns_interest:
            field = record.INFO.get(col)
            if field and ',' in str(field):
                tmp_list = field.split(',')
            else:
                tmp_list = [field]
            # print(col, tmp_list)
            if '_pred' not in col:
                tmp_list = [np.nan if (x=='.' or x==None) else float(x) for x in tmp_list]
                value = np.nanmax(tmp_list)
                if np.isnan(value):
                    index_dict[col] = np.nan
                else:
                    index = tmp_list.index(value)
                    index_dict[col] = index
            if '_pred' in col:
                tmp_list = [np.nan if x=='.' else x for x in tmp_list]
                scorecol = str(col).replace('_pred','_score')
                if np.isnan(index_dict[scorecol]):
                    value = np.nan
                else:
                    value = tmp_list[index_dict[scorecol]]
            tmp_dict[col] = value


        '''
        try:
            tmp_list = [np.nan if x=='.' else float(x) for x in tmp_list]
            value = np.nanmax(tmp_list)
            index = tmp_list.index(value)
            index_dict[col] = index
        except:
            tmp_list = [np.nan if x=='.' else x for x in tmp_list]
            if '_pred' in str(col):
                scorecol = str(col).replace('_pred','_score')
                value = tmp_list[index_dict[scorecol]]

        tmp_dict[col] = value
        '''
        # e+=1 
        # if e == 6:
        #     exit()
        l.append(tmp_dict)
    print('Unselected variants : ' + str(j))
    final_df = pd.DataFrame(data=l)
    # final_df = pd.DataFrame(data=l, columns=['ID'] + ['True_Label'] + columns_interest)

    print(final_df.isna().sum())
    # print(final_df)
    # print(final_df.dropna())
    # exit()

    # final_df.to_csv(path_or_buf=output, index=False, compression='gzip', sep='\t')

    list_of_str = ['SNV', 'A', 'T', 'C', 'G', 'U']
    # final_df = final_df[~final_df['CADD_RAW'].isin(list_of_str)]
    # final_df = final_df[~final_df['CADD_PHRED'].isin(list_of_str)]
    # final_df = final_df[~final_df['LRT_pred'].isin(list_of_str)]
    # final_df = final_df.dropna()
    for col in final_df:
        col_name_pred = col + '_pred'
        # if col in thresholds_less:
    
        #     final_df[col] = pd.to_numeric(final_df[col])
        #     final_df[col_name_pred] = np.where(final_df[col] < thresholds_less[col], '1', '-1')
        # # try:

        # if col in thresholds_sup:
        #     try:
        #         final_df[col] = pd.to_numeric(final_df[col])
        #         final_df[col_name_pred] = np.where(final_df[col] > thresholds_sup[col], '1', '-1')
        #     except:
        #         pass
        if col in dict_convert:
            final_df = final_df.replace({col : dict_convert[col]})
    
        # except:
        #     pass
    # print(final_df)

    col_ordered = ['ID', 'True_Label'] + list(sorted(set(list(final_df.columns)) - set(['ID', 'True_Label'])))
    final_df = final_df[col_ordered]
    # final_df = final_df.replace('', np.nan)
    # print(final_df)
    # print(final_df.dropna())
    na = final_df.isna().sum()
    na.to_csv('na.csv')
    # final_df = final_df.dropna()
    # final_df = final_df.drop_duplicates(subset='ID', keep=False)

    print(final_df)
    print('Pathogenic number : ' + str(len(final_df.loc[final_df['True_Label'] == '1'])))
    print('Begnin number : ' + str(len(final_df.loc[final_df['True_Label'] == '-1'])))
    #final_df.dropna(inplace=True)
    final_df.to_csv(path_or_buf=output, index=False, compression='gzip', sep='\t')


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='From VCF to CSV', usage='%(prog)s [-h] [-i INI]')

    required = parser.add_argument_group('required arguments')

    required.add_argument('-f', '--file_name',
                          metavar='',
                          type=str,
                          required=True,
                          help='Name of the VCF file')

    required.add_argument('-o', '--output_name',
                        type=str,
                        required=True,
                        help='Name of the output pandas file')

    parser.add_argument('-c', '--columns_of_interest',
                        metavar='',
                        nargs="*",
                        type=str,
                        help='Which values of INFO.CSQ features you want to extract')

    parser.add_argument('-l', '--label',
                        metavar='',
                        type=str,
                        help='Label')

    args = parser.parse_args()
    arg_dict = vars(args)
    final = parse(fname=arg_dict['file_name'],
                output=arg_dict['output_name'],
                  columns_interest=arg_dict['columns_of_interest'],
                  true_label=arg_dict['label']
                  )
