from pprint import pprint
from sklearn.externals import joblib
from VarClass_utils import utils, ML, plots_and_outputs
import glob
import matplotlib.pyplot as plt
import os
import pandas as pd


class TestingClassification(object):
    def __init__(self,
                 input_data,
                 output_dir,
                 model_dir,
                 strategy,
                 standardize,
                 fill_blanks,
                 logger,
                 threshold
                 ):
        """
        Init
        Args:
            input_data:
            output_dir:
            model_dir:
            plot:
            logger:
        """
        utils.mkdir(output_dir)
        self.logger = logger
        print('\n')
        print('=' * 100)
        self.logger.info('You will TEST the trained model on selected data : {}'
                         .format(os.path.basename(input_data)))
        print('=' * 100)
        print('\n')
        utils.mkdir(output_dir)
        df = utils.prepare_input_data(input_data=input_data,
                                      output_dir=output_dir,
                                      logger=self.logger,
                                      strategy=strategy,
                                      standardize=standardize,
                                      fill_blanks=fill_blanks,
                                      )
        df = df.reset_index(drop=True)
        logger.info('TESTING on {} samples'.format(df.shape[0]))
        # TODO : Handle Models directory name
        if model_dir.endswith("Models"):
            model = model_dir
        elif 'Model' in model_dir:
            model = model_dir
        else:
            model = model_dir + "/TRAIN/Models"
        classifiers = self.load_classifiers(model_dir=model)
        output_dir = output_dir + "/TEST"
        utils.mkdir(output_dir)
        self.launch(data=df,
                    classifiers=classifiers,
                    input_file=input_data,
                    output_dir=output_dir,
                    threshold=threshold)

    @staticmethod
    def load_classifiers(model_dir):
        """

        Args:
            model_dir:

        Returns:

        """
        # os.chdir(model_file)
        classifiers = list()
        for mod in glob.glob(model_dir + "/*.pkl"):
            sk_model = joblib.load(mod)
            classifiers.append(sk_model)
        return classifiers

    def launch(self, data, classifiers, input_file, output_dir, threshold):
        """
        Launch classifiers evaluation and allow to save output results

        Args:
            input_file:
            threshold:
            data: Pandas Dataframe
                Dataframe with the preprocessed data corresponding to the selected mode
                (complete data, selected predictors)
            classifiers: list
                List of classifiers tested
            output_dir: str
                Name of the output directory
            plot: bool
                If enable, save the different results plots into the output directory

        Returns: None

        """
        self.logger.info('Encoding data...')
        encode_data, labels, classes, predicted_labels, le = ML.encode(data)
        info = data[["ID", "True_Label"]]
        log_list = list()
        results_proba = dict()
        results_pred = dict()
        predictors = list(data[data.columns.drop(list(data.filter(regex='pred')))].drop(['ID', 'True_Label'], axis=1).columns)
        fi = pd.DataFrame()
        utils.mkdir(output_dir + '/Reports')

        for clf in classifiers:
            self.logger.info('Testing classifier - {}'.format(clf.__class__.__name__))
            name = clf.__class__.__name__
            train_predictions_proba = clf.predict_proba(encode_data.values)
            # feat_imp = pd.Series(clf.feature_importances_, predictors).sort_values(ascending=False)
            # print(feat_imp)
            pred_proba = train_predictions_proba[:, 1]
            pred_adj = ML.adjusted_classes(pred_proba, threshold)
            results_pred[name] = le.inverse_transform(pred_adj)
            tmp_log = ML.compute_metrics(clf, pred_adj, pred_proba, labels)
            log_list.append(tmp_log)
            results_proba[clf.__class__.__name__] = pred_proba
            try:
                tmp_fi = pd.Series(clf.feature_importances_, predictors).sort_values(ascending=False).to_frame()
                tmp_fi.columns = [str(name)]
                tmp_fi = (100. * tmp_fi / tmp_fi.sum()).round(2).astype(str) + '%'
                fi = pd.concat([fi, tmp_fi], axis=1)
            except:
                pass
        fi.to_csv(output_dir + '/Reports/Feature_importances.csv', sep='\t', index=True)
        log_mean = pd.concat(log_list)
        log = log_mean.sort_values(by=['Accuracy'], ascending=False)
        log = log.reset_index(drop=True)
        self.logger.info('Dumping labels and proba predictions ...')
        ML.dump_prediction(results_proba, results_pred, info, output_dir, data)
        plots_and_outputs.print_stdout(log, input_file, output_dir,
                                       predicted_labels, le.inverse_transform(labels), data)
        utils.mkdir(output_dir + "/Plots")
        self.logger.info('Saving plots and results...')
        # plots_and_outputs.plot_accuracy(log, output_dir)
        # plots_and_outputs.plot_log_loss(log, output_dir)
        # plots_and_outputs.plot_auc_score(log, output_dir)
        plots_and_outputs.plot_roc_curve_testing(labels, results_proba, output_dir, predicted_labels, labels)
        plots_and_outputs.plot_precision_recall_curve_testing(labels, results_proba, output_dir)
