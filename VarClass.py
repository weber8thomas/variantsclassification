from colorama import init
from datetime import datetime
from pprint import pprint
from pyfiglet import figlet_format
from sklearn.externals import joblib
from termcolor import cprint
from VarClass_scripts.testing import TestingClassification
from VarClass_scripts.training import TrainingClassification
from VarClass_utils import utils, ML
import argparse
import glob
import numpy as np
import os
import pandas as pd
import pathlib
import sys
import warnings

# Sklearn algorithms import
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis, QuadraticDiscriminantAnalysis
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier, ExtraTreesClassifier
from sklearn.linear_model import LogisticRegression, SGDClassifier, LinearRegression, PassiveAggressiveClassifier
from sklearn.model_selection import RandomizedSearchCV
from sklearn.naive_bayes import GaussianNB, MultinomialNB
from sklearn.neighbors import KNeighborsClassifier, RadiusNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.svm import SVC, NuSVC, LinearSVC
from sklearn.tree import DecisionTreeClassifier

init(strip=not sys.stdout.isatty())

warnings.filterwarnings("ignore", category=DeprecationWarning)
warnings.simplefilter("always")

__project__ = 'VarClass '
__author__ = 'Thomas'
__copyright__ = ''
__credits__ = []  # include people who  reportedbeginug fixes, made suggestions, endtc.
__licence__ = ''  # GPL, MIT endtc
__maintainer__ = 'Thomas'  # shouldbegine people who will  fixbeginugs and make improvements if important
__email__ = 'thomas.weber@etu.unistra.fr'
__date__ = ''
__status__ = 'Prototype'  # 'Prototype', 'Development' or 'Production'
__note__ = ''


def training(ARGS):
    TrainingClassification(input_data=ARGS['input'],
                           classifiers=classifiers,
                           standardize=bool(ARGS['standardize']),
                           fill_blanks=ARGS['fill_blanks'],
                           strategy=ARGS['fill_blanks_strategy'],
                           output=ARGS["output_dir"],
                           logger=logger,
                           cv=ARGS['cross_validation']
                           )


def testing(ARGS):
    TestingClassification(input_data=ARGS['input'],
                          standardize=ARGS['standardize'],
                          fill_blanks=ARGS['fill_blanks'],
                          strategy=ARGS['fill_blanks_strategy'],
                          output_dir=ARGS["output_dir"],
                          model_dir=ARGS['model'],
                          logger=logger,
                          threshold=ARGS['threshold']
                          )


def training_and_testing(ARGS):
    if not os.path.exists(ARGS['output_dir'] + '/TRAIN/training.csv.gz') or not os.path.exists(ARGS['output_dir'] + '/TEST/testing.csv.gz'):
        logger.warn('--train_and_test mode selected but training and testing file not found, creation with following parameters :'
                    '--ratio : ' + str(ARGS['ratio']) + ', --proportion : ' + str(ARGS['proportion']))
        ARGS['force'] = True
    if os.path.exists(ARGS['output_dir'] + '/TRAIN/training.csv.gz') or os.path.exists(
            ARGS['output_dir'] + '/TEST/testing.csv.gz'):
        logger.info('Training and testing file found')

    if ARGS['force'] is True:
        utils.mkdir(ARGS['output_dir'])
        utils.mkdir(ARGS['output_dir'] + '/TRAIN')
        utils.mkdir(ARGS['output_dir'] + '/TEST')
        logger.warn('Creating new files or overwriting old ones')
        prop = ARGS['proportion']
        t = float(round(prop / (1 - prop), 2))

        ratio = ARGS['ratio']
        tmp = pd.read_csv(filepath_or_buffer=ARGS['input'],
                          sep='\t',
                          compression='gzip',
                          encoding='utf-8')

        complete_data_path = tmp.loc[tmp['True_Label'] == 1]
        complete_data_path = complete_data_path.sample(frac=1)
        complete_data_begn = tmp.loc[tmp['True_Label'] == -1]
        complete_data_begn = complete_data_begn.sample(frac=1)
        max_size = max(complete_data_path.shape[0], complete_data_begn.shape[0])
        min_size = min(complete_data_path.shape[0], complete_data_begn.shape[0])
        if max_size > (t * min_size):
            max_size = min_size * t
        elif max_size < (t * min_size):
            min_size = max_size / t
        if min_size < 1000 and min(complete_data_path.shape[0], complete_data_begn.shape[0]) == \
                complete_data_path.shape[0]:
            logger.warn('CAREFUL : Size of the pathogenic dataset will be < 1000 samples')
        eval_test_size = ratio
        train_path = complete_data_path.head(n=int(round(min_size * (1 - eval_test_size))))
        train_begn = complete_data_begn.head(n=int(round(max_size * (1 - eval_test_size))))
        # eval_path = complete_data_path.tail(n=int(round(min_size * eval_test_size)))
        # eval_begn = complete_data_begn.tail(n=int(round(max_size * eval_test_size)))
        eval_path = complete_data_path.tail(n=int(round(min_size * eval_test_size)))
        eval_begn = complete_data_begn.tail(n=int(round(min_size * eval_test_size)))

        complete_training = pd.concat([train_path, train_begn]).drop_duplicates(keep=False)
        complete_eval = pd.concat([eval_path, eval_begn]).drop_duplicates(keep=False)
        complete_training.to_csv(path_or_buf=ARGS['output_dir'] + '/TRAIN/training.csv.gz',
                                 sep='\t',
                                 compression='gzip',
                                 encoding='utf-8',
                                 index=False)

        complete_eval.to_csv(path_or_buf=ARGS['output_dir'] + '/TEST/testing.csv.gz',
                             sep='\t',
                             compression='gzip',
                             encoding='utf-8',
                             index=False)

    TrainingClassification(input_data=ARGS['output_dir'] + '/TRAIN/training.csv.gz',
                           classifiers=classifiers,
                           standardize=ARGS['standardize'],
                           fill_blanks=ARGS['fill_blanks'],
                           strategy=ARGS['fill_blanks_strategy'],
                           output=ARGS["output_dir"],
                           logger=logger,
                           cv=ARGS['cross_validation']
                           )

    TestingClassification(input_data=ARGS['output_dir'] + '/TEST/testing.csv.gz',
                          standardize=ARGS['standardize'],
                          fill_blanks=ARGS['fill_blanks'],
                          strategy=ARGS['fill_blanks_strategy'],
                          output_dir=ARGS["output_dir"],
                          model_dir=ARGS['model'],
                          logger=logger,
                          threshold=ARGS['threshold']
                          )


def prediction(ARGS):
    data = utils.prepare_input_data(input_data=ARGS['input'],
                                         standardize=ARGS['standardize'],
                                         fill_blanks=ARGS['fill_blanks'],
                                         strategy=ARGS['fill_blanks_strategy'],
                                         output_dir=ARGS["output_dir"],
                                         logger=logger,
                                         pred=True
                                         )
    data = data.reset_index(drop=True)

    classifiers_l = list()
    model_dir = ARGS['model']
    for mod in glob.glob(model_dir + "/*.pkl"):
        sk_model = joblib.load(mod)
        classifiers_l.append(sk_model)

    encode_data, labels, classes, predicted_labels, le = ML.encode(data)

    info = data[["ID", "True_Label"]]

    results_prob = dict()
    results_pred = dict()

    for c in classifiers_l:
        logger.info('Testing classifier - {}'.format(c.__class__.__name__))
        name = c.__class__.__name__
        train_predictions_prob = c.predict_proba(encode_data.values)
        train_predictions_prob = train_predictions_prob[:, 1]
        results_prob[name] = train_predictions_prob
        pred_adj = ML.adjusted_classes(train_predictions_prob, arg_dict['threshold'])
        results_pred[name] = pred_adj
    
    logger.info('Dumping proba and scores ... - {}'.format(c.__class__.__name__))

    ML.dump_prediction(results_prob, results_pred, info, arg_dict['output_dir'], data)


if __name__ == "__main__":

    text = "Welcome to VARCLASS"
    cprint(figlet_format(text, font="standard"), "blue")

    starttime = datetime.now()

    parser = argparse.ArgumentParser(description='Software designed to build ML models based on Scikit-Learn',
                                     usage='%(prog)s [--help]')

    required = parser.add_argument_group('Required arguments')

    mode = parser.add_argument_group('VarClass modes')

    optional = parser.add_argument_group('VarClass options')

    mode.add_argument('--train',
                      action='store_true',
                      help='Train algorithms on selected data')

    mode.add_argument('--train_and_test',
                      action='store_true',
                      help='Train and Test the selected algorithms on some data')

    mode.add_argument('--test',
                      action='store_true',
                      help='Test selected algorithms on some data based on saved models')

    mode.add_argument('--prediction',
                      action='store_true',
                      help='Predict only results without plots on some data based on saved models')

    required.add_argument('-i', '--input',
                          metavar='',
                          type=str,
                          required=True,
                          help='Input file')

    optional.add_argument('-o', '--output_dir',
                          type=str,
                          metavar='',
                          help='Output directory, default = Current directory')

    optional.add_argument('-f', '--force',
                          action='store_true',
                          help='If enable, create new training and testing sets from input data')

    optional.add_argument('-m', '--model',
                          metavar='',
                          type=str,
                          help='Model directory, required if --test or --prediction enabled')

    optional.add_argument('-prop', '--proportion',
                          type=float,
                          metavar='',
                          default=0.5,
                          help='If enable, selected proportion will be maintained between classes')

    optional.add_argument('-std', '--standardize',
                          action='store_true',
                          help='Standardize data with scikit-learn.preprocessing.StandardScaler, default=False')

    optional.add_argument('-fb', '--fill_blanks',
                          action='store_true',
                          help='Fill blanks or not, default=False')

    optional.add_argument('-fbs', '--fill_blanks_strategy',
                          type=str,
                          metavar='',
                          default="median",
                          help='"mean", "median", "most_frequent" or "constant",check : ')

    optional.add_argument('-clf', '--classifiers',
                          type=str,
                          nargs="*",
                          metavar='',
                          help='Give a list of specific classifiers selected to test on the dataset')

    optional.add_argument('-g', '--grid',
                          action='store_true',
                          help='If enable, perform randomized grid search for tuning hyperparameters')


    optional.add_argument('--n_iteration',
                          type=int,
                          metavar='',
                          default=10,
                          help='Number of n-iteration to perform during randomized grid search, '
                               'Larger is the number, longer is the script duration, default = 10')

    optional.add_argument('-cv', '--cross_validation',
                          type=int,
                          metavar='',
                          default=1,
                          help='Number of cross-validation to perform, '
                               'proportion between classes are conserved, default = 1')

    optional.add_argument('-cv_gs', '--cross_validation_grid_search',
                      type=int,
                      metavar='',
                      default=3,
                      help='Number of cross-validation to perform during grid search optimization, '
                           'proportion between classes are conserved, default = 1')


    optional.add_argument('-r', '--ratio',
                          type=float,
                          metavar='',
                          default=0.2,
                          help='Ratio between training and testing datasets, '
                               'default = 0.2 of the input file will be assigned to testing set')

    optional.add_argument('-t', '--threshold',
                          type=float,
                          metavar='',
                          default=0.5,
                          help='Custom threshold to classify binary data')

    optional.add_argument('--full_clf',
                          action='store_true',
                          help='If enable, test all the classifiers available')

    optional.add_argument('--threads',
                          type=int,
                          metavar='',
                          default=1,
                          help='Enable parallelization of specific algorithms (example : LogisticRegression)'
                               'To use all cores available on your hardware, specify -1'
                          )

    args = parser.parse_args()

    arg_dict = vars(args)

    classifiers = [
        GradientBoostingClassifier(),
        RandomForestClassifier(),
        # LogisticRegression(n_jobs=arg_dict['threads'], solver='newton-cg')
    ]

    parameters_dict = {
        'MLPClassifier': {'solver': ['lbfgs'],
                          'max_iter': [500, 1000, 1500],
                          'alpha': 10.0 ** -np.arange(1, 7),
                          'hidden_layer_sizes': np.arange(5, 12),
                          'random_state': [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
                          # 'verbose' : True
                          },
        'RandomForestClassifier': {"max_depth": [3, None],
                                   "max_features": [1, 3, 10],
                                   "min_samples_split": [2, 3, 10],
                                   "bootstrap": [True, False],
                                   "criterion": ["gini", "entropy"],
                                   'n_estimators': [10, 50, 100, 200],
                                   # 'verbose': True
                                   },
        'LogisticRegression': {'solver': ['lbfgs', 'newton-cg'],
                               'penalty': ['l2'],
                               'C': np.logspace(0, 4, 10),
                               'max_iter': [1000, 1500],
                               # 'verbose': True,
                               },
        'GradientBoostingClassifier': {"loss": ["deviance"],
                                       "learning_rate": [0.01, 0.025, 0.05, 0.075, 0.1, 0.15, 0.2],
                                       "min_samples_split": np.linspace(0.1, 0.5, 12),
                                       "min_samples_leaf": np.linspace(0.1, 0.5, 12),
                                       "max_depth": [3, 5, 8],
                                       "max_features": ["log2", "sqrt"],
                                       "criterion": ["friedman_mse", "mae"],
                                       "subsample": [0.5, 0.618, 0.8, 0.85, 0.9, 0.95, 1.0],
                                       "n_estimators": [10],
                                       # 'verbose': True,
                                       },
        'LinearDiscriminantAnalysis': {'solver': ['svd', 'lsqr', 'eigen', ],
                                       # 'verbose': True,
                                       },

    }

    full_classifiers = [
        # AdaBoostClassifier(),
        # DecisionTreeClassifier(),
        # ExtraTreesClassifier(n_jobs=arg_dict['threads'], n_estimators=100),
        # GaussianNB(),
        GradientBoostingClassifier(),
        # KNeighborsClassifier(n_jobs=arg_dict['threads']),
        # LinearDiscriminantAnalysis(),
        # LinearRegression(n_jobs=arg_dict['threads']),
        # LinearSVC(),
        LogisticRegression(),
        # MLPClassifier(),
        # PassiveAggressiveClassifier(n_jobs=arg_dict['threads']),
        # QuadraticDiscriminantAnalysis(),
        RandomForestClassifier(),
        # SGDClassifier(n_jobs=arg_dict['threads'], loss="log", max_iter=1000, tol=1e-3),
        # SVC(kernel="rbf", C=0.025, probability=True),
    ]

    rdm_clf = list()

    for clf in full_classifiers:
        rdm_clf.append(
            RandomizedSearchCV(
                clf,
                param_distributions=parameters_dict[clf.__class__.__name__],
                n_iter=arg_dict['n_iteration'],
                cv=arg_dict['cross_validation_grid_search'],
                scoring='roc_auc',
                n_jobs=arg_dict['threads'],
                # verbose=10
            )
        )
    if arg_dict['grid'] is True:
        full_classifiers = rdm_clf

    logger = utils.setup_custom_logger("Classification")

    # Conditions

    if arg_dict["classifiers"] is not None and len(arg_dict["classifiers"]) == 0:
        logger.error('Classifiers option selected but no one was given')
        sys.exit("============\nSee you soon :)\n============")

    if arg_dict["classifiers"] is not None and len(arg_dict["classifiers"]) > 0:
        classifiers = list()
        for clf in arg_dict["classifiers"]:
            try:
                classifiers.append(class_dict[clf])
            except NameError:
                logger.error('Unknown Classifier')
                sys.exit("============\nSee you soon :)\n============")

    if arg_dict["output_dir"] is None:
        cwd = os.getcwd()
        output_dir = cwd + '/Classification_Output'
        arg_dict["output_dir"] = output_dir

    if not os.path.exists(arg_dict["output_dir"]) and arg_dict["output_dir"] is not None:
        try:
            pathlib.Path(arg_dict["output_dir"]).mkdir(exist_ok=True)
        except FileNotFoundError:
            logger.error('Unable to find or create this output directory')
            sys.exit("============\nSee you soon :)\n============")

    if arg_dict['input'] is not None:
        test = os.path.isfile(arg_dict['input'])
        if test is True:
            pass
        if test is False:
            logger.error('File not exists')
            sys.exit("============\nSee you soon :)\n============")

    if arg_dict['fill_blanks_strategy'] is not None \
            and arg_dict['fill_blanks_strategy'] not in ['mean', 'median', 'constant', 'most_frequent']:
        logger.error('Strategy selected not exists')
        sys.exit("============\nSee you soon :)\n============")

    if arg_dict['full_clf'] is True:
        classifiers = full_classifiers

    if arg_dict['model'] is None and arg_dict['train_and_test'] is True:
        arg_dict['model'] = arg_dict["output_dir"]

    # LAUNCHING MODES

    if arg_dict["train"] is True and (arg_dict["test"] or arg_dict["train_and_test"]) is False:
        training(arg_dict)

    elif arg_dict['prediction'] is True:
        prediction(arg_dict)

    elif arg_dict["train_and_test"] is True \
            and (arg_dict["test"] or arg_dict["train"]) is False:
        training_and_testing(arg_dict)

    elif arg_dict["test"] is True and (arg_dict["train_and_test"] or arg_dict["train"]) is False:
        testing(arg_dict)

    elif (arg_dict["train_and_test"] and arg_dict["train"] and arg_dict["test"]) is False:
        logger.error('No mode selected')
        sys.exit("============\nSee you soon :)\n============")

    else:
        logger.error('Two modes selected or input file missing')
        sys.exit("============\nSee you soon :)\n============")
